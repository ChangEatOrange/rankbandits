#!/bin/sh

#SBATCH --mem=1G
#SBATCH -c2
#SBATCH -o ./out/%A_%a.out                     
#SBATCH -e ./out/%A_%a.err
#SBATCH -p gpu,mem,cpu
#SBATCH --time=100:00:00
#SBATCH --array=1-100%20

cd /zfs/ilps-plex1/slurm/datastore/cli1
source ./anaconda/bin/activate bandit_venv

cd /zfs/ilps-plex1/slurm/datastore/cli1/rankbandits/rankbandits/

HPARAMS_FILE=hparams-yahoo.txt
# touch hparams-yahoo.txt
# for i in {0..4000}; do echo "--data_idx=$i" >> hparams-yahoo.txt; done
#python letor_simulation.py --data_idx=$1 --user=$2
python letor_simulation.py --dataset_idx=0 --interloop=$2  --user=$1 $(head -$SLURM_ARRAY_TASK_ID $HPARAMS_FILE | tail -1)

source deactivate
