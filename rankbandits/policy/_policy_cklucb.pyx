cimport cython
import numpy as np
cimport numpy as np
from scipy import optimize
import sys

from libc.math cimport log as c_log
from libc.math cimport sqrt as c_sqrt
from libc.math cimport abs as c_abs
from libc.stdio cimport printf

ctypedef np.float64_t DOUBLE
ctypedef np.int32_t INT

np.import_array()

cdef double EPS = 1e-6
cdef double GAMMA = 1e-3


cdef inline double double_max(double a, double b): return a if a >= b else b
cdef inline double double_min(double a, double b): return a if a <= b else b

cdef double KL_kappa(double mu, double p, double kappa):
    if mu == 0:
        return - c_log(1 - kappa * p)
    if mu == 1:
        return - c_log(kappa * p)
    return mu * c_log(mu /(kappa * p)) + (1 - mu) * c_log((1 - mu)/(1 - kappa * p))

cdef double linear_kl(double x,
                      np.ndarray[DOUBLE, ndim=1] theta_hat,
                      np.ndarray[DOUBLE, ndim=1] N_plays_l,
                      np.ndarray[DOUBLE, ndim=1] kappa,
                      double f_t):
    cdef:
        Py_ssize_t L = kappa.shape[0]
        Py_ssize_t l
        double res = 0
    for l in range(L):
        res += N_plays_l[l] * KL_kappa(theta_hat[l], x, kappa[l])
    return res - f_t

cdef double linear_kl_diff(double x,
                           np.ndarray[DOUBLE, ndim=1] theta_hat,
                           np.ndarray[DOUBLE, ndim=1] N_plays_l,
                           np.ndarray[DOUBLE, ndim=1] kappa,
                           double f_t):
    cdef:
        Py_ssize_t L = kappa.shape[0]
        Py_ssize_t l
        double res = 0
    for l in range(L):
        res += N_plays_l[l] * (- theta_hat[l] / x + kappa[l] * (1 - theta_hat[l]) \
                / (1 - kappa[l] * x))
    return res

cpdef np.ndarray[DOUBLE, ndim=1] _computeCKLUCB(
        int t,
        np.ndarray[DOUBLE, ndim=2] gains,
        np.ndarray[DOUBLE, ndim=2] N_plays,
        np.ndarray[DOUBLE, ndim=1] kappa,
        int n_iter):
    cdef:
        Py_ssize_t k
        Py_ssize_t K = N_plays.shape[0]
        Py_ssize_t L = N_plays.shape[1]
        double u_m = EPS
        double u_M = 1 - EPS
        double u_avg = 0
        double f_t = (1 + GAMMA) * c_log(t)
        np.ndarray[np.float64_t, ndim=2] theta_hat = np.zeros(
                (K, L), dtype=np.float64)
        np.ndarray[DOUBLE, ndim=1] res = np.zeros(K, dtype=np.float64)

    theta_hat = gains / N_plays

    for k in range(K):
        u_m = EPS
        u_M = 1 - EPS
        if linear_kl(u_M, theta_hat[k], N_plays[k], kappa, f_t) < 0:
            res[k] = u_M
            continue
        for i in range(n_iter):
            u_avg = (u_m + u_M) / 2
            if linear_kl_diff(u_avg, theta_hat[k], N_plays[k], kappa, f_t) < 0:
                u_m = u_avg
            else:
                if linear_kl(u_avg, theta_hat[k], N_plays[k], kappa, f_t) < 0:
                    u_m = u_avg
                else:
                    u_M = u_avg
        res[k] = (u_m + u_M) / 2
    return res
