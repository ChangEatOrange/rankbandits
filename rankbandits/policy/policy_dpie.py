#-*-coding: utf-8 -*-

import numpy as np
from scipy import optimize
import sys
from policy import Policy

from _policy_cklucb import _computeCKLUCB


DELTA = 1e-8
EPS = 1e-6

class PolicyDPIE(Policy):

    def __init__(self, L, T, kappa, stats=False):
        self.L = L
        self.T = T
        self.kappa = kappa
        self.stats = stats
        self.initialized = False

    def selectArms(self, L):
        """
        This functions selects L arms among the K ones depending on their
        KL-UCBs and empirical means.

        Output:
        -------
        chosen_arms: list of length L
            Indices of the chosen arms ordered by decreasing respective UCBs

        Reference:
        ----------
            Learning to Rank: Regret Lower Bounds and Efficient Algorithms
            R. Combes, S. Magureanu, A. Proutières, C. Laroche
        """
        if not self.initialized:
            return None     # Better raise an error
        # 1. Compute new empirical means
        #self.means = self.gains / self.N_plays
        N_tilde = (self.big_kappa * self.N_plays).sum(axis=1)
        self.means = np.minimum(self.gains.sum(axis=1) / N_tilde, 1)
        # 2. Compute new KL-UCB indices
        #self.UCB = _computeKLUCB(self.t, self.gains, self.N_plays, 20)
        self.UCB = _computeCKLUCB(self.t, self.gains, self.N_plays, self.kappa, 20)
        # 3. Compute new L_cal (L best empirical means)
        self.L_cal = list(self.means.argsort()[::-1][:L])
        # 4. Compute new B_cal
        L_cal_set = set(self.L_cal)
        mean_position_L = self.means[self.L_cal[-1]]
        #best_klucb = -1.
        for arm, klucb in enumerate(self.UCB):
            if klucb > mean_position_L and arm not in L_cal_set:
                self.B_cal.add(arm)
        #        cur = arm
        #        best_klucb = klucb
        if self.B_cal and np.random.uniform() > 0.5:        # Not empty and > 1/2
            self.L_cal[-1] = np.random.choice(list(self.B_cal))   # Exploration on the last slot
        #self.L_cal.append(cur)
        chosen_arms = self.L_cal
        self.nPlaysStatistics(chosen_arms)
        return chosen_arms

    def nPlaysStatistics(self, chosen_arms):
        if self.stats:
            for l, a in enumerate(chosen_arms):
                self.Ni[a][l][self.t] += 1

    def updateState(self, arms, rewards):
        self.B_cal = set()
        for l, (a, r) in enumerate(zip(arms, rewards)):
            self.N_plays[a][l] += 1
            self.gains[a][l] += r
        self.t += 1

    def init(self, K):
        self.K = K
        self.t = 0                      # Number of rounds played
        self.UCB = np.zeros(self.K)     # UCBs
        self.N_plays = np.zeros((self.K, self.L))     # Current count
        self.gains = np.zeros((self.K, self.L))       # Rewards a every position
        self.big_kappa = np.zeros((self.K,self.L))
        for k in range(self.K):
            self.big_kappa[k] += self.kappa
        self.B_cal = set()              # Items with indices > L-th emp. mean
        self.L_cal = None

        # Statistics: [arm] x [pos] x [time]
        if self.stats:
            self.Ni = np.zeros((self.K, self.L, self.T))

    def __str__(self):
        return 'S-PIE policy'

    @staticmethod
    def id():
        return 'D-PIE'

    @staticmethod
    def recquiresInit():    # Policies which recquire an initialization
        return True
