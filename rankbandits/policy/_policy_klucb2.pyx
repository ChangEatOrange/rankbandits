cimport cython
import numpy as np
cimport numpy as np
import sys

from libc.math cimport log as c_log
from libc.math cimport abs as c_abs
from libc.stdio cimport printf

ctypedef np.float64_t DOUBLE
ctypedef np.int32_t INT

np.import_array()

cdef double DELTA = 1e-8
cdef double EPS = 1e-6

cdef inline double double_max(double a, double b): return a if a >= b else b
cdef inline double double_min(double a, double b): return a if a <= b else b

cdef double KLBernoulli(double p, double q):
    if p >= 1 or q >= 1:
        -1
    return p * c_log(p / q) + (1 - p) * c_log((1 - p) / (1 - q))

cdef double derivativeBernoulli(double p, double q):
    return (p - q) / (q * (1 - q))


cpdef np.ndarray[DOUBLE, ndim=1] _computeKLUCB(int n,
        int n_iter,
        np.ndarray[DOUBLE, ndim=1] gains,
        np.ndarray[DOUBLE, ndim=1] N_plays):
    cdef:
        Py_ssize_t i
        Py_ssize_t j
        double upper_bound = 0
        double p = 0
        double q = 0
        cdef Py_ssize_t K = gains.shape[0]
        np.ndarray[DOUBLE, ndim=1] res = np.zeros(
                K, dtype=np.float64)

    for i in range(K):
        upper_bound = c_log(n) / N_plays[i]
        p = double_max(gains[i] / N_plays[i], DELTA)
        if p >= 1:
            res[i] = 1
            continue
        q = p + EPS
        j = 0
        while j < (n_iter+50):
            j += 1
            f = upper_bound - KLBernoulli(p, q)
            f_diff = derivativeBernoulli(p, q)
            if c_abs(f) < EPS:
                break
            q = double_max(p + EPS, double_min(1 - DELTA, q - f / f_diff))
        res[i] = q
    return res
