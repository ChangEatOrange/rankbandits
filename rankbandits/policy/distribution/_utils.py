import numpy as np
from mpmath import mp
import sys

EPS = 1e-2
N_POINTS = 100
n_iter = 50

def posterior_ts(x, plays, kappa):
    x = x * mp.mpf(1)
    res = 1
    for l in range(len(kappa)):
        res *= x**(plays[l][0]-1)*(1-kappa[l]*x)**(plays[l][1]-1)
    return res

"""
Generates samples from posterior obtained by ambiguous feedback using grid
"""
def _posterior_ts_sample2(N_plays, means, kappa, norm):
    K = len(means)
    L = len(kappa)
    res = np.zeros(K)
    for k in range(K):
        # We generate a sample
        points = np.linspace(0, 1, N_POINTS)
        unorm_post = posterior_ts(points, N_plays[k], kappa)
        normal_constant = sum(unorm_post) / N_POINTS
        norm[k] = normal_constant
        #means[k] = (left + right) / 2
        posterior = [float(num) for num in unorm_post / sum(unorm_post)]
        res[k] = np.random.choice(points, size=1, p=posterior)
    return res

def _posterior_ts_sample(N_plays, means, kappa, norm):
    K = len(means)
    L = len(kappa)
    res = np.zeros(K)
    for k in range(K):
        # We compute the left bound of the interval
        i = 0
        if (posterior_ts(0, N_plays[k], kappa) / norm[k]) > EPS:
            left = 0
            i = n_iter
        while (posterior_ts(means[k], N_plays[k], kappa) / norm[k]) < EPS:
            print posterior_ts(means[k], N_plays[k], kappa) / norm[k]
            print N_plays[k]
            print norm[k]
            print means[k]
            print "Probleme"
            sys.exit(1)
        m = 0
        M = means[k]
        while i < n_iter:
            i += 1
            left = (m + M) / 2
            if (posterior_ts(left, N_plays[k], kappa) / norm[k]) < EPS:
                m = left
            else:
                M = left
        # We compute the right bound of the  interval
        i = 0
        if (posterior_ts(1, N_plays[k], kappa) / norm[k]) > EPS:
            right = 1
            i = n_iter
        m = means[k]
        M = 1
        while i < n_iter:
            i += 1
            right = (m + M) / 2
            if (posterior_ts(right, N_plays[k], kappa) / norm[k]) < EPS:
                M = right
            else:
                m = right
        # Now we generate a sample
        points = np.linspace(left, right, N_POINTS)
        unorm_post = posterior_ts(points, N_plays[k], kappa)
        normal_constant = sum(unorm_post) * (right - left) / N_POINTS
        norm[k] = normal_constant
        means[k] = (left + right) / 2
        posterior = [float(num) for num in unorm_post / sum(unorm_post)]
        res[k] = np.random.choice(points, size=1, p=posterior)
    return res
