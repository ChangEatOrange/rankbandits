#-*-coding: utf-8 -*-

from beta import BetaCollection
from _real_beta import rejection_sampling

__all__ = ['BetaCollection',
           'rejection_sampling',
           ]
