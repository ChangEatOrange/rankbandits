import numpy as np


def n_swap_to_optimal(ranked_score):
    return np.sum([np.sum(score < ranked_score[idx + 1:]) for idx, score in enumerate(ranked_score[:-1])])


class CascadeUser(object):
    def __init__(self, p_click, p_pos, p_stop):

        self.p_click = np.asarray(p_click)
        self.p_stop = np.asarray(p_stop)
        self.p_pos = np.asarray(p_pos)
        self.L = len(p_click)
        self.K =len(self.p_stop)
        self.l_star = np.argsort(-self.p_click)

        self.ndcg_weights = np.asarray([np.log(2) / np.log(idx + 2) for idx in range(self.K)])
        self.idcgs_rel = self.p_click[self.l_star][:self.K] * self.ndcg_weights # ideal discounted attractive probability
        self.idcgs_power_rel = np.power(2, self.p_click[self.l_star][:self.K]) * self.ndcg_weights
        self.v0 = n_swap_to_optimal(self.p_click)
        self.conservative_constraint = self.v0 + self.K / 2.

    def set_attraction(self, p_click):
        self.p_click = np.asarray(p_click)
        self.l_star = np.argsort(-self.p_click)
        self.ndcg_weights = np.asarray([np.log(2) / np.log(idx + 2) for idx in range(self.K)])
        self.idcgs_rel = self.p_click[self.l_star][:self.K] * self.ndcg_weights  # ideal discounted attractive probability
        self.idcgs_power_rel = np.power(2, self.p_click[self.l_star][:self.K]) * self.ndcg_weights
        self.v0 = n_swap_to_optimal(self.p_click)
        self.conservative_constraint = self.v0 + self.K / 2.

    def randomize(self):
        # sample random variables
        self.vt = np.array(np.random.rand(self.L) < self.p_click, dtype=np.int)
        
    def get_click(self, arms):
        # reward of arms (chosen items)
        self.randomize()
        r = self.vt[arms]
        if r.sum() > 0:
            first_click = np.flatnonzero(r)[0]
            r[first_click + 1:] = 0
        return r
        
    def regret(self, arms):
        # regret of arms (chosen items)
        l_star = self.l_star[:len(arms)]
        return np.prod(1 - self.vt[arms]) - np.prod(1 - self.vt[l_star])
        
    def pregret(self, arms):
        # expected regret of arms (chosen items)
        l_star = self.l_star[:len(arms)]
        return np.prod(1 - self.p_click[arms]) - np.prod(1 - self.p_click[l_star])

    def ndcg_rel(self, arms):
        n_pos = len(arms)
        dcg = self.p_click[arms] * self.ndcg_weights[:n_pos]
        return np.sum(dcg) / np.sum(self.idcgs_rel[:n_pos])

    def ndcg_power_rel(self, arms):
        n_pos = len(arms)
        dcg = np.power(2, self.p_click[arms]) * self.ndcg_weights[:n_pos]
        return np.sum(dcg) / np.sum(self.idcgs_power_rel[:n_pos])

    def is_valid(self, arms):
        return n_swap_to_optimal(self.p_click[arms]) > self.conservative_constraint

    def is_valid_to_baselien(self, arms):
        return n_swap_to_optimal(self.p_click[arms]) > self.v0

    @property
    def attraction(self):
        return self.p_click

    @property
    def n_items(self):
        return len(self.p_click)

    @property
    def kappa(self):
        return self.p_pos

    @property
    def stop_prob(self):
        return self.p_stop


class DCMUser(CascadeUser):
    def __init__(self, p_click, p_pos, p_stop):
        '''
        In DCM we essentially cares user satisfactory.
        :param p_click: with the length of self.L
        :param p_pos: should be void
        :param p_stop: should be equal to the number of positions
        '''
        super(DCMUser, self).__init__(p_click, p_pos, p_stop)

    def randomize(self):
        # sample random variables
        self.vt = np.array(np.random.rand(self.L) < self.p_click, dtype=np.int)
        self.coin_stop = np.asarray(np.random.rand(self.K) < self.p_stop, dtype=np.int)

    def get_click(self, arms):
        # return the number of clicks
        self.randomize()
        r = self.vt[arms]
        coin_satisfy = self.coin_stop * r
        if coin_satisfy.sum() > 0:
            first_satisfy = np.flatnonzero(r)[0]
            r[first_satisfy+1:] = 0
        return r

    def regret(self, arms):
        '''
        :param arms:
        :return: the probability of satisfaction of the ranked list
        '''
        # regret of arms (chosen items)
        l_star = self.l_star[:len(arms)]
        coin_stop = self.coin_stop[:len(arms)]
        return np.prod(1 - coin_stop*self.vt[arms]) - np.prod(1 - coin_stop*self.vt[l_star])

    def pregret(self, arms):
        # expected regret of arms (chosen items)
        l_star = self.l_star[:len(arms)]
        p_stop = self.p_stop[:len(arms)]
        return np.prod(1 - p_stop*self.p_click[arms]) - np.prod(1 - p_stop*self.p_click[l_star])


class NonsataionaryCascadeUser(CascadeUser):
    def __init__(self, p_click, p_pos, p_stop, period=int(1e3), new_attraction=.9):
        """

        :param p_click:
        :param p_pos:
        :param p_stop:
        :param period: length of the constant epoch
        :param new_attraction:
        """
        super(NonsataionaryCascadeUser, self).__init__(p_click, p_pos, p_stop)
        self.original_p_click = np.asarray(p_click[:])
        self.period = period
        self.new_attraction = new_attraction
        self.t = 0
        self.nonstationary_item = np.argsort(self.p_click)[:self.L-self.K]

    def get_click(self, arms):
        # reward of arms (chosen items)
        if self.t % self.period == 0:
            new_p_click = self.original_p_click.copy()
            if self.t % (2*self.period) != 0:
                idx = np.random.choice(self.nonstationary_item, self.K, replace=False) if len(
                    self.nonstationary_item) >= self.K else self.nonstationary_item
                new_p_click[idx] = self.new_attraction
            self.set_attraction(new_p_click)
        self.t += 1
        self.randomize()
        r = self.vt[arms]
        if r.sum() > 0:
            first_click = np.flatnonzero(r)[0]
            r[first_click + 1:] = 0
        return r


class PBMUser(object):
    def __init__(self, p_click, p_pos, p_stop):
        self.p_click = np.asarray(p_click)
        self.p_pos = np.asarray(p_pos)
        self.p_stop = np.asarray(p_stop)
        self.L = len(self.p_click)
        self.K =len(self.p_pos)
        self.l_star = np.argsort(-self.p_click)
        self.l_star = np.argsort(-self.p_click)[:self.K][np.argsort(np.lexsort((np.arange(self.K), -self.p_pos)))]
        self.ndcg_weights = np.asarray([np.log(2) / np.log(idx + 2) for idx in range(self.K)])
        self.idcgs_rel = self.p_click[self.l_star][
                         :self.K] * self.ndcg_weights  # ideal discounted attractive probability
        self.idcgs_power_rel = np.power(2, self.p_click[self.l_star][:self.K]) * self.ndcg_weights
        self.v0 = n_swap_to_optimal(self.p_click)
        self.conservative_constraint = self.v0 + self.K / 2.

    def set_attraction(self, p_click):
        self.p_click = np.asarray(p_click)
        self.l_star = np.argsort(-self.p_click)
        self.ndcg_weights = np.asarray([np.log(2) / np.log(idx + 2) for idx in range(self.K)])
        self.idcgs_rel = self.p_click[self.l_star][
                         :self.K] * self.ndcg_weights  # ideal discounted attractive probability
        self.idcgs_power_rel = np.power(2, self.p_click[self.l_star][:self.K]) * self.ndcg_weights
        self.v0 = n_swap_to_optimal(self.p_click)
        self.conservative_constraint = self.v0 + self.K / 2.

    def randomize(self):
        # sample random variables
        self.ut = np.array(np.random.rand(self.K) < self.p_pos, dtype=np.int)
        self.vt = np.array(np.random.rand(self.L) < self.p_click, dtype=np.int)
        
    def get_click(self, arms):
        self.randomize()
        # reward of arms (chosen items)
        return np.multiply(self.ut[:len(arms)], self.vt[arms])
        
    def regret(self, arms):
        # regret of arms (chosen items)
        l = len(arms)
        return np.dot(self.ut[:l], self.vt[self.l_star[:l]]) - np.dot(self.ut[:l], self.vt[arms])
        
    def pregret(self, arms):
        # expected regret of arms (chosen items)
        l = len(arms)        
        return np.dot(self.p_pos[:l], self.p_click[self.l_star[:l]]) - np.dot(self.p_pos[:l], self.p_click[arms[:l]])

    def ndcg_rel(self, arms):
        n_pos = len(arms)
        dcg = self.p_click[arms] * self.ndcg_weights[:n_pos]
        return np.sum(dcg) / np.sum(self.idcgs_rel[:n_pos])

    def ndcg_power_rel(self, arms):
        n_pos = len(arms)
        dcg = np.power(2, self.p_click[arms]) * self.ndcg_weights[:n_pos]
        return np.sum(dcg) / np.sum(self.idcgs_power_rel[:n_pos])

    def is_valid(self, arms):
        return n_swap_to_optimal(self.p_click[arms]) > self.conservative_constraint

    def is_valid_to_baselien(self, arms):
        return n_swap_to_optimal(self.p_click[arms]) > self.v0

    @property
    def attraction(self):
        return self.p_click
    @property
    def n_items(self):
        return len(self.p_click)

    @property
    def kappa(self):
        return self.p_pos

    @property
    def statisfication(self):
        return self.p_stop


class NonsataionaryPBMUser(PBMUser):
    def __init__(self, p_click, p_pos, p_stop, period=int(1e3), new_attraction=.9):
        super(NonsataionaryPBMUser, self).__init__(p_click, p_pos, p_stop)
        self.original_p_click = np.asarray(p_click[:])
        self.period = period
        self.new_attraction = new_attraction
        self.t = 1
        self.nonstationary_item = np.argsort(self.p_click)[:self.L-self.K]

    def get_click(self, arms):
        # reward of arms (chosen items)
        if self.t % self.period == 0:
            new_p_click = self.original_p_click.copy()
            if self.t % (2*self.period) != 0:
                idx = np.random.choice(self.nonstationary_item, self.K, replace=False) if len(
                    self.nonstationary_item) >= self.K else self.nonstationary_item
                new_p_click[idx] = self.new_attraction
            self.set_attraction(new_p_click)
        self.t += 1
        self.randomize()
        # reward of arms (chosen items)
        return np.multiply(self.ut[:len(arms)], self.vt[arms])


if __name__ == '__main__':
    kappa = [0.99997132, 0.95949374, 0.76096783]#, 0.59179909, 0.45740329, 0.38584302, 0.33052186, 0.28372475, 0.26211924, 0.26700303]
    p_stop = [0.6555304, 0.4868164, 0.46051615]#, 0.46315161, 0.45642676, 0.47130397, 0.50317268, 0.54764235, 0.65359742, 0.99998025]
    cp = [0.4370, 0.5729, 0.1579, 0.2258, 0.1392, 0.1446, 0.0938, 0.0400, 0.0682, 0.0238]

    l = len(cp)
    k = len(kappa)
    user = NonsataionaryPBMUser(cp, kappa, p_stop, period=int(1e2), new_attraction=.9)

    for i in range(1000):
        user.get_click([0, 1, 2])
        if i % 100 == 0:
            print user.attraction