#!/bin/sh

#SBATCH --mem=2G
#SBATCH -c2
#SBATCH -o ./out/%A_%a.out
#SBATCH -e ./out/%A_%a.err
#SBATCH --time=24:00:00
#SBATCH --array=1-100%34

# if [ $# = 2 ]
#then
#	python run_click_bandits.py --data_idx=$1 --user=$2 --iteration=1000 --scale=1 --repeat=1000
#else
#python run_click_bandits.py --data_idx=$1 --user=$2
#fi

# touch hparams-yandex.txt
# for i in {1..100}; do echo "--data_idx=$i" >> hparams-yandex.txt; done

source activate py2
HPARAMS_FILE=hparams-yandex.txt

python run_click_bandits.py --user=$1 $(head -$SLURM_ARRAY_TASK_ID $HPARAMS_FILE | tail -1)


conda deactivate
