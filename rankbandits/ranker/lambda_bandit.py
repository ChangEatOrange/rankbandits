import numpy as np
from abstract_rank import AbstractRank
import logging


class LambdaDCM(AbstractRank):
    def __init__(self, items, kappa, policy, c=3, T=1e5):
        super(LambdaDCM, self).__init__(items, kappa, policy, c, T)
        self.w = np.zeros((self.L, self.L))
        self.w_self = np.zeros(self.L)
        self.n_self = np.ones(self.L)

    def get_arms(self, l=-1):
        lam = self.w.sum(axis=1) + self.w_self
        n = (self.w + self.w.T).sum(axis=1) + self.n_self
        #
        # if self.t * self.k < self.L:
        #     self.r_idx = np.argsort(n)[:self.k]
        #     return self.r_idx
        bound = self.policy(lam, n, self.t, self.c)
        aux_score = np.random.rand(self.L)
        idx = np.lexsort((aux_score, -bound))
        self.r_idx = idx[:self.K]
        return self.r_idx

    def update(self, ck, arms=None):
        self.t += 1
        zero_idx = [item == 0 for item in ck]
        if np.sum(ck) == 0:
            self.n_self[self.r_idx] += 1
        elif np.sum(ck) == self.K:
            self.w_self[self.r_idx] += 1
            self.n_self[self.r_idx] += 1
        else:
            for idx, item in enumerate(ck):
                if item == 1:
                    beat_idx = np.where(zero_idx[:idx])[0]
                    if len(beat_idx) == 0:
                        self.w_self[self.r_idx[idx]] += 1
                        self.n_self[self.r_idx[idx]] += 1
                    else:
                        self.w[self.r_idx[idx], self.r_idx[beat_idx]] += 1

    def get_stat(self):
        lam = self.w.sum(axis=1) + self.w_self
        n = (self.w + self.w.T).sum(axis=1) + self.n_self
        return lam/n, self.policy(lam, n, self.t, self.c)


# future work we can take satisfication into account
class LambdaPBM(AbstractRank):
    def __init__(self, items, kappa, policy, c=3, T=1e5):
        super(LambdaPBM, self).__init__(items, kappa, policy, c, T)
        self.w = np.zeros((self.L, self.L))
        self.w_self = np.zeros(self.L)
        self.n_self = np.ones(self.L)

    def get_arms(self, l=-1):
        lam = self.w.sum(axis=1) + self.w_self
        n = (self.w + self.w.T).sum(axis=1) + self.n_self
        #
        # if self.t * self.k < self.L:
        #     self.r_idx = np.argsort(n)[:self.k]
        #     return self.r_idx
        bound = self.policy(lam, n, self.t, self.c)
        aux_score = np.random.rand(self.L)
        idx = np.lexsort((aux_score, -bound))
        self.r_idx = idx[:self.K]
        return self.r_idx

    def update(self, ck, arms=None):
        ck = np.asarray(ck)
        self.t += 1
        if np.sum(ck) == 0:
            self.n_self[self.r_idx] += self.kappa
        elif np.sum(ck) == self.K:
            self.w_self[self.r_idx] += 1
        else:
            clicked = np.where(ck == 1)[0]
            non_click = np.where(ck == 0)[0]
            for idx, item in enumerate(clicked):
                self.w[self.r_idx[item], self.r_idx[non_click]] += (self.kappa[non_click])

    def get_stat(self):
        lam = self.w.sum(axis=1) + self.w_self
        n = (self.w + self.w.T).sum(axis=1) + self.n_self
        return lam/n, self.policy(lam, n, self.t, self.c)


# future work we can take satisfication into account
