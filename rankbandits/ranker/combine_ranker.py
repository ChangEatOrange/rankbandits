import numpy as np
from cascading_bandits import CascadingBandits
from bubble_rank import BubbleRank
from abstract_rank import AbstractRank


class CombineRankerFreq(AbstractRank):
    #  Choose the initial ranker by empirical average.

    def __init__(self, items, kappa, policy, c=3, T=1e6, initial_step=20000):
        super(CombineRankerFreq, self).__init__(items, kappa, policy, c, T)
        self.initial_step = initial_step
        self.initial_ranker = CascadingBandits(items, kappa, policy, c)
        self.bubble = BubbleRank(items, self.kappa, policy, self.c)
        self.initial_ranking = []

    def get_arms(self, l=-1):
        if self.t < self.initial_step:
            self.initial_ranking = self.initial_ranker.get_arms()
            return self.initial_ranking
        elif self.t == self.initial_step:
            self.initial_ranking = np.argsort(-self.initial_ranker.get_stat()[0])
            return self.initial_ranking
        else:
            return self.initial_ranking[self.bubble.get_arms()]

    def update(self, ck, arms=None):
        if self.t <= self.initial_step:
            self.initial_ranker.update(ck, arms)
        else:
            self.bubble.update(ck)
        self.t += 1


class CombineRanker(AbstractRank):
    #  Choose the initial ranker by UCB.

    def __init__(self, items, kappa, policy, c=3, T=1e6, initial_step=20000):
        super(CombineRanker, self).__init__(items, kappa, policy, c, T)
        self.initial_step = initial_step
        self.initial_ranker = CascadingBandits(items, kappa, policy, c)
        self.bubble = BubbleRank(items, self.kappa, policy, self.c)
        self.initial_ranking = []

    def get_arms(self, l=-1):
        if self.t <= self.initial_step:
            self.initial_ranking = self.initial_ranker.get_arms()
            return self.initial_ranking
        else:
            return self.initial_ranking[self.bubble.get_arms()]

    def update(self, ck, arms=None):
        if self.t <= self.initial_step:
            self.initial_ranker.update(ck, arms)
        else:
            self.bubble.update(ck)
        self.t += 1


class CombineRankerPos(AbstractRank):
    #  Choose the initial ranker by frequency of appearing at the position.

    def __init__(self, items, kappa, policy, c=3, T=1e6, initial_step=20000):
        super(CombineRankerPos, self).__init__(items, kappa, policy, c, T)
        self.initial_step = initial_step
        self.initial_ranker = CascadingBandits(items, kappa, policy, c)
        self.bubble = BubbleRank(items, self.kappa, policy, self.c)
        self.initial_ranking = []
        self.frequency = np.random.rand(self.L, self.K)
        self.tmp_idx = range(self.K)

    def get_arms(self, l=-1):
        if self.t < self.initial_step:
            self.initial_ranking = self.initial_ranker.get_arms()
            self.frequency[self.initial_ranking, self.tmp_idx] += 1
            return self.initial_ranking
        elif self.t == self.initial_step:
            self.initial_ranking = self.initial_ranker.get_arms()
            self.frequency[self.initial_ranking, self.tmp_idx] += 1
            self.initial_ranking = self.frequency.argmax(axis=0)
            print self.initial_ranking
            return self.initial_ranking
        else:
            return self.initial_ranking[self.bubble.get_arms()]

    def update(self, ck, arms=None):
        if self.t <= self.initial_step:
            self.initial_ranker.update(ck, arms)
        else:
            self.bubble.update(ck)
        self.t += 1