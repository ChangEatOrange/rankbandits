from abstract_rank import AbstractRank
from _klucb import klucb as kb
from _kllcb import kllcb as kl
import numpy as np


def klucb(w, n, t, c=3, precision=1e-6):
    return kb(w, n, t, c, precision)


def kllcb(w, n, t, c=3, precision=1e-6):
    return kl(w, n, t, c, precision)


class Batch(object):
    def __init__(self, I, B, T):
        self.I = I # positions
        self.B = B # type list; items in batch
        self.T = T # horizon
        self.n_pos = 1 + self.I[1] - self.I[0]
        self.n_arms = len(B)
        self.items = dict() # relative index
        [self.items.update({self.B[i]: i_t}) for i, i_t in enumerate(np.random.permutation(self.n_arms))]
        self.l = 1 #stage
        self.nl = np.ceil(16 * 4 ** self.l * np.log(self.T))
        self.c = np.zeros(self.n_arms)
        self.n = np.zeros(self.n_arms)

    def ranked(self):
        return self.n_arms == self.n_pos

    def display(self):
        if self.ranked():
            return self.I, np.random.permutation(self.B)
        else:
            d_idx = np.argsort(self.n)[:self.n_pos]
            return self.I, np.random.permutation([self.B[i] for i in d_idx])

    def collect_click(self, ck, arms):
        for idx, arm in enumerate(arms[self.I[0]:(self.I[1]+1)]):
            self.n[self.items[arm]] += 1
            self.c[self.items[arm]] += ck[idx+self.I[0]]

    def update(self, t):
        rt = None
        if self.n.min() >= self.nl and self.n_pos > 1:
            ucb = klucb(self.c, self.n, self.T)
            lcb = kllcb(self.c, self.n, self.T)
            idx_sort = np.argsort(-lcb)
            Bp = []
            Bm = []
            for i in range(1, self.n_pos):
                Bp.append(idx_sort[:i])
                Bm.append(idx_sort[i:])
            s = 0
            for i in range(self.n_pos-1):
                if lcb[idx_sort[i]] > ucb[Bm[i]].max():
                    # print ucb[Bm[i]]
                    # print Bm[i]
                    # print 'i', i, 'guider' , self.B[idx_sort[i]], 'lcb', lcb[idx_sort[i]]
                    s = i + 1
                    # print t

            if s == 0 and self.n_arms >= self.n_pos:
                # elimination
                survivors = np.where(ucb >= lcb[idx_sort[self.n_pos - 1]])[0]
                self.reset(survivors, reset_counter=False)
                self.nl = np.ceil(16 * 4 ** self.l * np.log(self.T))
            else:
                # split
                I_new = [self.I[0] + s , self.I[1]]
                # I_new = [self.I[0], self.I[0] + s -1]
                self.I[1] =  self.I[0] + s -1
                B_new = [self.B[i_b] for i_b in idx_sort[s:]]
                self.reset(idx_sort[:s], reset_counter=True)
                self.l = 1
                self.nl = np.ceil(16 * 4 ** self.l * np.log(self.T))
                rt = Batch(I_new, B_new, self.T)
            return rt

    def reset(self, survivors, reset_counter):
        new_item = dict()
        [new_item.update({self.B[it]: idx}) for idx, it in enumerate(survivors)]
        self.items = new_item
        new_B = [self.B[i] for i in survivors]
        self.B = new_B
        self.n_arms = len(self.B)
        if reset_counter:
            self.n = np.zeros(self.n_arms)
            self.c = np.zeros(self.n_arms)
        else:
            self.n = self.n[survivors]
            self.c = self.c[survivors]
        self.n_pos = 1 + self.I[1] - self.I[0]

    def cb(self, t):
        ucb = klucb(self.c, self.n, t)
        lcb = kllcb(self.c, self.n, t)
        return ucb, lcb


class BatchRank(AbstractRank):
    def __init__(self, items, kappa, policy=None, c=3, T=1e6):
        items = np.random.permutation(items)
        super(BatchRank, self).__init__(items, kappa, policy, c, T)
        # only for KLUCB policy
        self.policy = klucb
        self.A = []#active arms
        self.A.append(Batch([0, self.K-1], self.items, self.T))
        self.bmax = len(self.A)

    def get_arms(self, l=None):
        self.r_idx = np.zeros(self.K, dtype=np.int)
        for b in self.A:
            idx, arms = b.display()
            self.r_idx[idx[0]:idx[1]+1] = arms
        return self.r_idx

    def update(self, ck, arms=None):
        if arms is None:
            arms = self.r_idx
        self.t += 1
        # update click
        [b.collect_click(ck, arms) for b in self.A]
        # update model
        a_tmp = []
        for b in range(self.bmax):
            b_tmp = self.A[b].update(self.t)
            if b_tmp is not None:
                a_tmp.append(b_tmp)
        self.A += a_tmp
        self.bmax = len(self.A)


