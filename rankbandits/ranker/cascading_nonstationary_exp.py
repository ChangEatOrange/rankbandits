import numpy as np
from cascading_nonstationary import CascadingNonStationary
from user import NonsataionaryCascadeUser


class CascadingRexp3(CascadingNonStationary):
    def __init__(self, items, kappa, delta, policy, c=0.1, T=1e6):
        """
        This is from the NIPS 14 paper. Besbes et al.
        :param items:
        :param kappa:
        :param delta: budget size in (1/self.L, 1/self.L * T)
        :param policy: not used in this case
        :param c: is gamma in the paper in (0, 1), not used
        :param T:
        : self.L number of items
        : self.K number of positions
        """
        super(CascadingRexp3, self).__init__(items, kappa, delta, policy, c, T=T)
        self.name = 'CascadingRexp3'
        self.batch_size = np.ceil(self.L ** (1./3) * (1.0 * self.T / delta) ** (2./3))
        self.batch_size = np.ceil(self.L * self.T ** (2. / 3))
        self.gamma = c + 0.0
        self.gamma = min(1, np.sqrt(self.L*np.log(self.L) / ((np.e - 1) * self.batch_size)))
        self.w = np.ones(self.L)

    def get_arms(self, l=-1):
        if l == -1:
            l = self.L
        w = self.w * 1.
        p = (1 - self.gamma) * self.w / np.sum(self.w) + self.gamma / self.L
        self.r_idx = np.random.choice(range(len(p)), size=len(p), replace=False, p=p)
        return self.r_idx

    def update(self, ck, arms=None):
        if arms is None:
            arms = self.r_idx

        self.t += 1

        if self.t % self.batch_size == 0:
            self.w = np.ones(self.L)
        else:
            p = (1 - self.gamma) * self.w / np.sum(self.w) + self.gamma / self.L
            c = np.where(ck == 1)[0]
            if c.size > 0:
                idx = arms[c][0]
                # p_aux = 1 - p[idx] - np.cumsum([0] + [tmp for tmp in p[arms[:c[0]]]])
                # p_aux = 1 - np.prod(p_aux)
                p_aux = p[idx]
                self.w[idx] *= np.exp(self.gamma / self.L / p_aux)


if __name__ == '__main__':
    cp = [0.4370, 0.5729, 0.1579, 0.2258, 0.1392, 0.1446, 0.0938, 0.0400, 0.0682, 0.0238]
    kappa = [0.99997132, 0.95949374, 0.76096783]
    p_stop = [0.6555304, 0.4868164, 0.46051615]  # , 0.46315161, 0.45642676]

    l = len(cp)
    k = len(kappa)

    n_top = 5
    T = 10000
    ranker = CascadingRexp3(range(l), kappa[:], 5, 'ucb', 0.1, T=T)

    user = NonsataionaryCascadeUser(cp, kappa, p_stop, 2000)

    regret = np.zeros(T)
    for i in range(T):
        arms = ranker.get_arms()
        ck = user.get_click(arms[:n_top])
        ranker.update(ck, arms[:n_top])
        regret[i] = user.pregret(arms[:n_top])
        if regret[i] < -0.001:
            print user.p_click, user.original_p_click
            print arms
    cum_regret = regret.cumsum()
    print cum_regret[::100]
