import numpy as np
from abstract_rank import AbstractRank


class BubbleRank(AbstractRank):
    def __init__(self, items, kappa, policy, c=3, T=1e6):
        super(BubbleRank, self).__init__(items, kappa, policy, c, T)
        assert self.L == self.K
        self.item_shown = []
        self.items = [{'id': id, 'c': np.ones(self.L), 'n': np.ones(self.L), 'beat': set([]), 'rank': rk} for rk, id
                      in enumerate(items)]
        self.ranking = items
        self.ucb = np.zeros(self.L - 1)

    def __swap_item(self, i, j):
        tmp = self.item_shown[i]
        self.item_shown[i] = self.item_shown[j]
        self.item_shown[j] = tmp

    def get_arms(self, l=-1):
        self.item_shown = []
        self.item_shown = [self.items[itm]['id'] for itm in self.ranking]
        h = self.t % 2
        coins = np.random.rand(self.K/2)
        [self.__swap_item(i, i+1) if coins[i/2] > 0.5 and self.item_shown[i+1] not in self.items[self.item_shown[i]]['beat']
         else None for i in range(h, self.K-1, 2)]
        self.item_shown = np.asarray(self.item_shown)
        return self.item_shown

    def update(self, ck, arms=None):
        if arms is None:
            arms = self.item_shown

        #
        swapped = False
        # statistics
        ck = np.asarray(ck)
        h = self.t % 2
        for idx in range(h, self.K-1, 2):
            if ck[idx] == ck[idx+1]:
                continue
            self.items[arms[idx]]['n'][arms[idx + 1]] += 1
            self.items[arms[idx + 1]]['n'][arms[idx]] += 1
            if ck[idx] == 1:
                self.items[arms[idx]]['c'][arms[idx + 1]] += 1
                self.items[arms[idx + 1]]['c'][arms[idx]] -= 1
            else:
                self.items[arms[idx + 1]]['c'][arms[idx]] += 1
                self.items[arms[idx]]['c'][arms[idx + 1]] -= 1
        self.t += 1

        # inner compare
        # to see whether item i is better than item i+1 wich high confidence
        u_c = np.asarray([self.items[self.ranking[idx]]['c'][self.ranking[idx + 1]] for idx in range(self.L - 1)])
        u_n = np.asarray([self.items[self.ranking[idx]]['n'][self.ranking[idx + 1]] for idx in range(self.L - 1)])
        u_e = u_c / u_n
        bound = 2.0*np.sqrt((np.log(self.T)) / u_n)
        for idx in range(self.L - 1)[::-1]:
            if u_e[idx] > bound[idx]:
                rk1 = self.ranking[idx]
                rk2 = self.ranking[idx + 1]
                self.items[rk1]['beat'].update([rk2])

        # to see wheter item 1+i is betther than item i
        l_c = np.asarray([self.items[self.ranking[idx]]['c'][self.ranking[idx - 1]] for idx in range(1, self.L)])
        l_n = np.asarray([self.items[self.ranking[idx]]['n'][self.ranking[idx - 1]] for idx in range(1, self.L)])
        l_e = l_c / l_n
        winner = [-1] * self.L
        for idx in range(0, self.L-1):
            if l_e[idx] > bound[idx]:
                winner[self.ranking[idx+1]] = self.ranking[idx]
        begin = -1
        for rk in self.ranking[::-1]:
            if winner[rk] != -1:
                stk = [rk]
                item = rk
                while winner[item] != -1:
                    stk.append(winner[item])
                    winner[item] = -1
                    item = stk[-1]
                self.items[rk]['beat'].update(stk)
                begin = self.items[rk]['rank'] - len(stk) + 1
                for rerank_idx in range(len(stk)):
                    self.ranking[rerank_idx + begin] = stk[rerank_idx]
                    self.items[stk[rerank_idx]]['rank'] = rerank_idx + begin
                    swapped = True

        return swapped, begin


class BubbleRankIPV(AbstractRank):
    # improved version of BubbleRanker. Assume the transitive relation 
    def __init__(self, items, kappa, policy, c=3, T=1e6):
        super(BubbleRankIPV, self).__init__(items, kappa, policy, c, T)
        assert self.L == self.K
        self.item_shown = []
        self.items = [{'id': it, 'c': np.ones(self.L), 'n': np.ones(self.L), 'beat': set([]), 'rank': it} for it
                      in items]
        self.ranking = items
        self.ucb = np.zeros(self.L - 1)

    def __swap_item(self, i, j):
        tmp = self.item_shown[i]
        self.item_shown[i] = self.item_shown[j]
        self.item_shown[j] = tmp

    def get_arms(self, l=-1):
        self.item_shown = []
        self.item_shown = [self.items[itm]['id'] for itm in self.ranking]
        h = self.t % 2
        coins = np.random.rand(self.K/2)
        [self.__swap_item(i, i+1) if coins[i/2] > 0.5 and self.item_shown[i+1] not in self.items[self.item_shown[i]]['beat']
         else None for i in range(h, self.K-1, 2)]
        self.item_shown = np.asarray(self.item_shown)
        return self.item_shown

    def update(self, ck, arms=None):
        if arms is None:
            arms = self.item_shown

        #
        swapped = False
        # statistics
        ck = np.asarray(ck)
        h = self.t % 2
        for idx in range(h, self.K-1, 2):
            if ck[idx] == ck[idx+1]:
                continue
            self.items[arms[idx]]['n'][arms[idx + 1]] += 1
            self.items[arms[idx + 1]]['n'][arms[idx]] += 1
            if ck[idx] == 1:
                self.items[arms[idx]]['c'][arms[idx + 1]] += 1
                self.items[arms[idx + 1]]['c'][arms[idx]] -= 1
            else:
                self.items[arms[idx + 1]]['c'][arms[idx]] += 1
                self.items[arms[idx]]['c'][arms[idx + 1]] -= 1
        self.t += 1

        # inner compare
        # to see whether item i is better than item i+1 wich high confidence
        u_c = np.asarray([self.items[self.ranking[idx]]['c'][self.ranking[idx + 1]] for idx in range(self.L - 1)])
        u_n = np.asarray([self.items[self.ranking[idx]]['n'][self.ranking[idx + 1]] for idx in range(self.L - 1)])
        u_e = u_c / u_n
        bound = np.sqrt((2.0*np.log(self.T)) / u_n)
        for idx in range(self.L - 1)[::-1]:
            if u_e[idx] > bound[idx]:
                rk1 = self.ranking[idx]
                rk2 = self.ranking[idx + 1]
                self.items[rk1]['beat'].update(self.items[rk2]['beat'].union([rk2]))

        # to see wheter item 1+i is betther than item i
        l_c = np.asarray([self.items[self.ranking[idx]]['c'][self.ranking[idx - 1]] for idx in range(1, self.L)])
        l_n = np.asarray([self.items[self.ranking[idx]]['n'][self.ranking[idx - 1]] for idx in range(1, self.L)])
        l_e = l_c / l_n
        winner = [-1] * self.L
        for idx in range(0, self.L-1):
            if l_e[idx] > bound[idx]:
                winner[self.ranking[idx+1]] = self.ranking[idx]
        begin = -1
        for rk in self.ranking[::-1]:
            if winner[rk] != -1:
                stk = [rk]
                item = rk
                while winner[item] != -1:
                    stk.append(winner[item])
                    winner[item] = -1
                    item = stk[-1]
                self.items[rk]['beat'].update(stk)
                begin = self.items[rk]['rank'] - len(stk) + 1
                for rerank_idx in range(len(stk)):
                    self.ranking[rerank_idx + begin] = stk[rerank_idx]
                    self.items[stk[rerank_idx]]['rank'] = rerank_idx + begin
                    swapped = True

        return swapped, begin


class BubbleRankKLUCB(AbstractRank):
    def __init__(self, items, kappa, policy, c=3, T=1e6):
        super(BubbleRankKLUCB, self).__init__(items, kappa, policy, c, T)
        assert self.L == self.K
        self.item_shown = []
        self.items = [{'id': it, 'c': np.zeros(self.L), 'n': np.ones(self.L), 'beat': set([]), 'rank': it} for it
                      in items]
        self.ranking = items
        self.ucb = np.zeros(self.L - 1)
        self.lcb = np.zeros(self.L - 1)

    def get_arms(self, l=-1):
        self.item_shown = []
        current_ranking = [self.items[rk]['id'] for rk in self.ranking]

        def swap_item(odd):
            if odd:
                for i in range(0, self.L - 1, 2):
                    if current_ranking[i + 1] in self.items[current_ranking[i]]['beat']:
                        self.item_shown.append(current_ranking[i])
                        self.item_shown.append(current_ranking[i + 1])
                    else:
                        self.item_shown.append(current_ranking[i + 1])
                        self.item_shown.append(current_ranking[i])
            else:
                self.item_shown.append(current_ranking[0])
                for i in range(1, self.L - 1, 2):
                    if current_ranking[i + 1] in self.items[current_ranking[i]]['beat']:
                        self.item_shown.append(current_ranking[i])
                        self.item_shown.append(current_ranking[i + 1])
                    else:
                        self.item_shown.append(current_ranking[i + 1])
                        self.item_shown.append(current_ranking[i])
            if len(self.item_shown) < self.L:
                self.item_shown.append(current_ranking[-1])

        current_ranking = [self.items[itm]['id'] for itm in self.ranking]
        if self.t % 3 == 0:
            self.item_shown = current_ranking
        elif self.t % 3 == 1:
            swap_item(odd=True)
        else:
            swap_item(odd=False)
        self.item_shown = np.asarray(self.item_shown)
        return self.item_shown

    def update(self, ck, arms=None):
        if arms is None:
            arms = self.item_shown
        # statistics
        ck = np.asarray(ck)
        if self.t % 3 == 0:
            for idx, rk in enumerate(arms):
                if idx != 0 and ck[idx] != ck[idx - 1]:
                    self.items[rk]['c'][arms[idx - 1]] += ck[idx]
                    self.items[rk]['n'][arms[idx - 1]] += 1
                if idx != self.L - 1 and ck[idx] != ck[idx + 1]:
                    self.items[rk]['c'][arms[idx + 1]] += ck[idx]
                    self.items[rk]['n'][arms[idx + 1]] += 1
        elif self.t % 3 == 1:
            for i in range(0, self.L - 1, 2):
                if ck[i] == ck[i + 1]:
                    continue
                self.items[arms[i]]['c'][arms[i + 1]] += ck[i]
                self.items[arms[i + 1]]['c'][arms[i]] += ck[i + 1]
                self.items[arms[i]]['n'][arms[i + 1]] += 1
                self.items[arms[i + 1]]['n'][arms[i]] += 1
        else:
            for i in range(1, self.L - 1, 2):
                if ck[i] == ck[i+1]:
                    continue
                self.items[arms[i]]['c'][arms[i + 1]] += ck[i]
                self.items[arms[i + 1]]['c'][arms[i]] += ck[i + 1]
                self.items[arms[i]]['n'][arms[i + 1]] += 1
                self.items[arms[i + 1]]['n'][arms[i]] += 1
        self.t += 1
        # inner compare
        if self.t % 3 == 0:
            u_c = np.asarray([self.items[self.ranking[idx]]['c'][self.ranking[idx + 1]] for idx in range(self.L - 1)])
            u_n = np.asarray([self.items[self.ranking[idx]]['n'][self.ranking[idx + 1]] for idx in range(self.L - 1)])
            l_c = np.asarray([self.items[self.ranking[idx]]['c'][self.ranking[idx - 1]] for idx in range(1, self.L)])
            l_n = np.asarray([self.items[self.ranking[idx]]['n'][self.ranking[idx - 1]] for idx in range(1, self.L)])

            # first we see whether the rank is perfect
            self.ucb = self.policy(l_c, l_n, self.t, self.c)
            self.lcb = self.lower(u_c, u_n, self.t, self.c)
            for idx in range(self.L - 1)[::-1]:
                if self.ucb[idx] < self.lcb[idx]:
                    rk1 = self.ranking[idx]
                    rk2 = self.ranking[idx+1]
                    self.items[rk1]['beat'].update(self.items[rk2]['beat'].union([rk2]))

            self.ucb = self.policy(u_c, u_n, self.t, self.c)
            self.lcb = self.lower(l_c, l_n, self.t, self.c)
            winner = [-1] * self.L
            for idx in range(self.L - 1):
                if self.ucb[idx] < self.lcb[idx]:
                    winner[self.ranking[idx + 1]] = self.ranking[idx]
            for rk in self.ranking[::-1]:
                if winner[rk] != -1:
                    stk = [rk]
                    item = rk
                    while winner[item] != -1:
                        stk.append(winner[item])
                        winner[item] = -1
                        item = stk[-1]
                    self.items[rk]['beat'].update(stk)
                    begin = self.items[rk]['rank'] - len(stk) + 1
                    for rerank_idx in range(len(stk)):
                        self.ranking[rerank_idx + begin] = stk[rerank_idx]
                        self.items[stk[rerank_idx]]['rank'] = rerank_idx + begin





# class BubbleRank(AbstractRank):
#     def __init__(self, items, kappa, policy, c=3, T=1e5):
#         super(BubbleRank, self).__init__(items, kappa, policy, c, T)
#         assert self.L == self.K
#         self.item_shown = []
#         self.items = [{'id': it, 'c': np.zeros(self.L), 'n': np.zeros(self.L), 'beat': set([]), 'rank': it} for it
#                       in items]
#         self.ranking = items
#         self.ucb = np.zeros(self.L - 1)
#         self.lcb = np.zeros(self.L - 1)
#
#     def get_arms(self, l=-1):
#         self.item_shown = []
#         current_ranking = [self.items[rk]['id'] for rk in self.ranking]
#
#         def swap_item(odd):
#             if odd:
#                 for i in range(0, self.L - 1, 2):
#                     if current_ranking[i + 1] in self.items[current_ranking[i]]['beat']:
#                         self.item_shown.append(current_ranking[i])
#                         self.item_shown.append(current_ranking[i + 1])
#                     else:
#                         self.item_shown.append(current_ranking[i + 1])
#                         self.item_shown.append(current_ranking[i])
#             else:
#                 self.item_shown.append(current_ranking[0])
#                 for i in range(1, self.L - 1, 2):
#                     if current_ranking[i + 1] in self.items[current_ranking[i]]['beat']:
#                         self.item_shown.append(current_ranking[i])
#                         self.item_shown.append(current_ranking[i + 1])
#                     else:
#                         self.item_shown.append(current_ranking[i + 1])
#                         self.item_shown.append(current_ranking[i])
#             if len(self.item_shown) < self.L:
#                 self.item_shown.append(current_ranking[-1])
#
#         current_ranking = [self.items[itm]['id'] for itm in self.ranking]
#         if self.t % 3 == 0:
#             self.item_shown = current_ranking
#         elif self.t % 3 == 1:
#             swap_item(odd=True)
#         else:
#             swap_item(odd=False)
#         self.item_shown = np.asarray(self.item_shown)
#         return self.item_shown
#
#     def update(self, ck, arms=None):
#         if arms is None:
#             arms = self.item_shown
#         # statistics
#         ck = np.asarray(ck)
#         if self.t % 3 == 0:
#             for idx, rk in enumerate(arms):
#                 if idx != 0:
#                     self.items[rk]['c'][arms[idx - 1]] += ck[idx]
#                     self.items[rk]['n'][arms[idx - 1]] += 1
#                 if idx != self.L - 1:
#                     self.items[rk]['c'][arms[idx + 1]] += ck[idx]
#                     self.items[rk]['n'][arms[idx + 1]] += 1
#         elif self.t % 3 == 1:
#             for i in range(0, self.L - 1, 2):
#                 self.items[arms[i]]['c'][arms[i + 1]] += ck[i]
#                 self.items[arms[i + 1]]['c'][arms[i]] += ck[i + 1]
#                 self.items[arms[i]]['n'][arms[i + 1]] += 1
#                 self.items[arms[i + 1]]['n'][arms[i]] += 1
#         else:
#             for i in range(1, self.L - 1, 2):
#                 self.items[arms[i]]['c'][arms[i + 1]] += ck[i]
#                 self.items[arms[i + 1]]['c'][arms[i]] += ck[i + 1]
#                 self.items[arms[i]]['n'][arms[i + 1]] += 1
#                 self.items[arms[i + 1]]['n'][arms[i]] += 1
#         self.t += 1
#         # inner compare
#         if self.t % 3 == 0:
#             u_c = np.asarray([self.items[self.ranking[idx]]['c'][self.ranking[idx + 1]] for idx in range(self.L - 1)])
#             u_n = np.asarray([self.items[self.ranking[idx]]['n'][self.ranking[idx + 1]] for idx in range(self.L - 1)])
#             l_c = np.asarray([self.items[self.ranking[idx]]['c'][self.ranking[idx - 1]] for idx in range(1, self.L)])
#             l_n = np.asarray([self.items[self.ranking[idx]]['n'][self.ranking[idx - 1]] for idx in range(1, self.L)])
#
#             # first we see whether the rank is perfect
#             self.ucb = self.policy(l_c, l_n, self.t, self.c)
#             self.lcb = self.lower(u_c, u_n, self.t, self.c)
#             for idx in range(self.L - 1)[::-1]:
#                 if self.ucb[idx] < self.lcb[idx]:
#                     rk1 = self.ranking[idx]
#                     rk2 = self.ranking[idx+1]
#                     self.items[rk1]['beat'].update(self.items[rk2]['beat'].union([rk2]))
#
#             self.ucb = self.policy(u_c, u_n, self.t, self.c)
#             self.lcb = self.lower(l_c, l_n, self.t, self.c)
#             winner = [-1] * self.L
#             for idx in range(self.L - 1):
#                 if self.ucb[idx] < self.lcb[idx]:
#                     winner[self.ranking[idx + 1]] = self.ranking[idx]
#             for rk in self.ranking[::-1]:
#                 if winner[rk] != -1:
#                     stk = [rk]
#                     item = rk
#                     while winner[item] != -1:
#                         stk.append(winner[item])
#                         winner[item] = -1
#                         item = stk[-1]
#                     self.items[rk]['beat'].update(stk)
#                     begin = self.items[rk]['rank'] - len(stk) + 1
#                     for rerank_idx in range(len(stk)):
#                         self.ranking[rerank_idx + begin] = stk[rerank_idx]
#                         self.items[stk[rerank_idx]]['rank'] = rerank_idx + begin
#
#
