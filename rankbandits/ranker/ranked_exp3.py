import numpy as np
from abstract_rank import AbstractRank
from user import CascadeUser
import logging


class RankedEXP3(AbstractRank):

    def __init__(self, items, kappa, policy, c=0.3, T=1e5):
        """

        :param items:
        :param kappa:
        :param policy:
        :param c: trade off c in (0, 1)
        :param T:
        """
        super(RankedEXP3, self).__init__(items, kappa, policy, T)
        self.mab = [EXP3(self.L, c) for i in range(self.K)]
        self.winner = []

    def get_arms(self, l=-1):
        winner = []
        for i in range(self.K):
            winner.append(self.mab[i].get_arm(winner))
        self.r_idx = np.asarray(winner)
        return self.r_idx

    def update(self, ck, arms=None):
        self.t += 1
        reward = ck
        [bandit.update(reward[i]) for i, bandit in enumerate(self.mab)]


class EXP3(object):
    def __init__(self, k, c=0.1):
        self.k = k
        self.c = c
        self.w = np.ones(self.k)
        self.winner = -1
        self.p = np.ones(k) / k
        self.real_k = k

    def get_arm(self, chosen):
        w = self.w.copy()
        w[chosen] = 0
        self.real_k = k = self.k - len(chosen)
        self.p = (1 - self.c) * w / np.sum(w) + self.c / k
        self.p[chosen] = 0
        self.winner = np.random.choice(range(len(self.p)), size=1, replace=False, p=self.p)[0]
        return self.winner

    def update(self, reward):
        reward = reward / self.p[self.winner]
        self.w[self.winner] = self.w[self.winner] * np.exp(self.c*reward/self.real_k)


if __name__ == '__main__':
    cp = [0.4370, 0.5729, 0.1579, 0.2258, 0.1392, 0.1446, 0.0938, 0.0400, 0.0682, 0.0238]
    kappa = [0.99997132, 0.95949374, 0.76096783]
    p_stop = [0.6555304, 0.4868164, 0.46051615]  # , 0.46315161, 0.45642676]

    l = len(cp)
    k = len(kappa)

    n_top = 5
    T = 10000
    ranker = RankedEXP3(range(l), kappa[:], 'ucb', 0.1, T=T)

    user = CascadeUser(cp, kappa, p_stop)

    regret = np.zeros(T)
    for i in range(T):
        arms = ranker.get_arms()
        ck = user.get_click(arms[:n_top])
        ranker.update(ck, arms[:n_top])
        regret[i] = user.pregret(arms[:n_top])
    cum_regret = regret.cumsum()
    print cum_regret[::100]