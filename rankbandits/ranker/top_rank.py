import numpy as np
from abstract_rank import AbstractRank


class TopRank(AbstractRank):
    def __init__(self, items, kappa, policy, c=3, T=1e6):
        """

        :param items: item ids.
        :param kappa: examination probability. only need to know the lenght. K
        :param policy: not used
        :param c: not used
        :param T:
        """
        super(TopRank, self).__init__(items, kappa, policy, c, T)
        L = len(items)
        self.L = L
        self.T = T

        self.item_shown = None  # remember the shown items.

        self.pulls = np.ones((L, L))  # number of pulls
        self.reward = np.zeros((L, L))  # cumulative reward

        self.G = np.ones((L, L), dtype=bool)
        self.P = np.zeros(L)
        self.P2 = np.ones((L, L))

    def rerank(self):
        Gt = (self.reward / self.pulls - 2 * np.sqrt(np.log(self.T) / self.pulls)) > 0
        if not np.array_equal(Gt, self.G):
            self.G = np.copy(Gt)

            Pid = 0
            self.P = - np.ones(self.L)
            while (self.P == -1).sum() > 0:
                items = np.flatnonzero(Gt.sum(axis=0) == 0)
                self.P[items] = Pid
                Gt[items, :] = 0
                Gt[:, items] = 1
                Pid += 1

            self.P2 = \
                (np.tile(self.P[np.newaxis], (self.L, 1)) == np.tile(self.P[np.newaxis].T, (1, self.L))).astype(float)

    def update(self, ck, arms=None):
        if arms is None:
            arms = self.item_shown
        clicks = np.zeros(self.L)
        clicks[arms] = ck

        M = np.outer(clicks, 1 - clicks) * self.P2
        self.pulls += M + M.T
        self.reward += M - M.T
        self.rerank()

    def get_arms(self, l=-1):
        if l == -1:
            l = self.K
        self.item_shown = np.argsort(self.P + 1e-6 * np.random.rand(self.L))[: l]
        return self.item_shown
