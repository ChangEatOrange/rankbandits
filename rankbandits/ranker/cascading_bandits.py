import numpy as np
from abstract_rank import AbstractRank
import logging


class CascadingBandits(AbstractRank):
    def __init__(self, items, kappa, policy, c=3):
        """ implementation of Cascading bandits paper
        :param items: items id
        :param kappa: probability of observing only used in PBM
        :param policy: ucb, klucb, ts
        :param c: bandit parameter
        """
        super(CascadingBandits, self).__init__(items, kappa, policy, c)

    def get_arms(self, l=-1):
        if l == -1:
            l = self.L
        score = self.policy(self.w, self.n, self.t, self.c)
        aux_score = np.random.rand(self.L)
        idx = np.lexsort((aux_score, -score))
        self.r_idx = idx[:self.K]
        return self.r_idx

    def update(self, ck, arms=None):
        if arms is None:
            arms = self.r_idx
        self.t += 1
        if np.sum(ck) == 0:
            c = self.K
        else:
            c = np.where(ck == 1)[0][0]
            self.w[self.r_idx[c]] += 1
        self.n[self.r_idx[:c+1]] += 1


