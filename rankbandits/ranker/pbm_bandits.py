import numpy as np
import scipy as sp
import pickle
from policy import PolicyAZUCB, PolicyAmbiguousThompsonSampling, PolicyDPIE


POLICY_DICT = {'ts': PolicyAmbiguousThompsonSampling, 'ucb': PolicyAZUCB, 'klucb': PolicyDPIE}


class PBMBandits(object):
    def __init__(self, itemid, kappa, policy=None, c=3, T=1e5):
        self.T = T
        self.K = len(itemid)
        self.L = len(kappa)
        self.turn = 0  # how many times selected item list
        self.itemid = itemid
        self.kappa = np.asarray(kappa)
        self.S = np.ones((self.K, self.L))
        self.N = np.ones((self.K, self.L))
        self.Nc = np.ones((self.K, self.L))
        self.r_idx = []
        self.policy = POLICY_DICT[policy](self.L, T, self.kappa, False)
        self.c = c
        self.policy.init(self.K)
        self.policy.N_plays += 1
        self.policy.initialized = True
        self.policy.t = 1

    def get_arms(self, required_num=-1):
        if required_num == -1:
            required_num = self.L
        self.r_idx = self.policy.selectArms(required_num)
        return np.asarray(self.r_idx)

    def update(self, feedback, selected_items=None):
        self.turn += 1
        if selected_items is None:
            selected_items = self.r_idx
        self.policy.updateState(selected_items, feedback)

    def get_stat(self):
        return -1

    def save(self, fname):
        with open(fname, 'wb') as f:
            pickle.dump(self, f)

    @classmethod
    def load(cls, fname):
        with open(fname, 'rb') as f:
            return pickle.load(f)


