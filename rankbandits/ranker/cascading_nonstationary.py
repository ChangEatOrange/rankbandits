import numpy as np
from abstract_rank import AbstractRank


class CascadingNonStationary(AbstractRank):
    def __init__(self, items, kappa, policy, delta=-1, c=3, T=1e5):
        """ implementation of Cascading bandits paper
        :param items: items id
        :param kappa: probability of observing only used in PBM
        :param policy: ucb, klucb, ts
        :param delta: number of abrupts, if -1, use heuristic setup
        :param c: bandit parameter
        """
        super(CascadingNonStationary, self).__init__(items, kappa, policy, c, T=T)
        self.name = 'CascadingNonStationary'
        self.delta = delta

    def get_arms(self, l=-1):
        if l == -1:
            l = self.L
        score = self.policy(self.w, self.n, self.t, self.c)
        aux_score = np.random.rand(self.L)
        idx = np.lexsort((aux_score, -score))
        self.r_idx = idx[:self.K]
        return self.r_idx

    def update(self, ck, arms):
        raise NotImplementedError


class CascadingWindow(CascadingNonStationary):
    def __init__(self, items, kappa, policy, delta, c=3, T=1e5):
        super(CascadingWindow, self).__init__(items, kappa, policy, delta, c, T)
        self.name = 'CascadingWindow'
        if delta == -1:
            self.delta = np.int(2 * np.sqrt(T * np.log(T)))
        else:
            self.delta = np.int(2*np.sqrt(T*np.log(T) / delta))
        self.w_window = np.zeros((self.L, self.delta))
        self.n_window = np.zeros((self.L, self.delta))
        self.t_window = 0

    def update(self, ck, arms=None):
        if arms is None:
            arms = self.r_idx
        self.t = min(1+self.t, self.delta)
        self.t_window = (self.t_window+1) % self.delta

        self.w_window[:, self.t_window] = 0
        self.n_window[:, self.t_window] = 0

        if np.sum(ck) == 0:
            c = self.K
        else:
            c = np.where(ck == 1)[0][0]
            self.w_window[self.r_idx[c], self.t_window] = 1
        self.n_window[self.r_idx[:c+1], self.t_window] = 1
        self.w = np.sum(self.w_window, axis=1)
        self.n = np.sum(self.n_window, axis=1)+.1


class CascadingDiscount(CascadingNonStationary):
    def __init__(self, items, kappa, policy, delta, c=3, T=1e5):
        super(CascadingDiscount, self).__init__(items, kappa, policy, delta, c, T)
        if delta == -1:
            self.delta = 1 - 0.25 * np.sqrt(1./ T)
        else:
            self.delta = 1-0.25*np.sqrt((delta+0.0)/T)
        self.name = 'CascadingDiscount'

    def update(self, ck, arms=None):
        if arms is None:
            arms = self.r_idx

        self.t = 1. + self.delta * self.t
        self.w *= self.delta
        self.n *= self.delta

        if np.sum(ck) == 0:
            c = self.K
        else:
            c = np.where(ck == 1)[0][0]
            self.w[self.r_idx[c]] += 1.
        self.n[self.r_idx[:c+1]] += 1.
