import numpy as np
from abstract_rank import AbstractRank
from utils import arg_max
import logging


class DuelingClick(AbstractRank):

    def __init__(self, items, kappa, policy, c=3, T=1e5):
        super(DuelingClick, self).__init__(items, kappa, policy, c, T)
        self.w = np.ones((self.L+1, self.L+1))

    def get_arms(self, l=-1):
        self.r_idx = []
        n = self.w.T + self.w
        score = self.policy(self.w.flat[:], n.flat[:], self.t, self.c).reshape(self.L+1, self.L+1)
        np.fill_diagonal(score, 0.5)
        copeland_score = (score > 0.5).sum(axis=1)
        # pure dueling
        first_arm = arg_max(copeland_score[:-1])
        reletiva_bound = score[:-1, first_arm]
        reletiva_bound[first_arm] = -1
        self.r_idx.append(first_arm)
        for i in range(self.K-1):
            r_arm = arg_max(reletiva_bound)
            self.r_idx.append(r_arm)
            reletiva_bound[r_arm] = -1

        # for i in range(self.K):
        #     # construct the list, based on the dueling bandit
        #     first_arm = arg_max(copeland_score[:-1])
        #     self.r_idx.append(first_arm)
        #     copeland_score = score[:, first_arm]
        #     copeland_score[self.r_idx] = -1

        self.r_idx = np.asarray(self.r_idx)
        return self.r_idx

    def update(self, ck, arms=None):
        self.t += 1
        if np.sum(ck) == 0:
            # the last one is a fake arm
            self.w[-1, self.r_idx] += 1
        elif np.sum(ck) == self.K:
            self.w[self.r_idx, -1] += 1
        else:
            clicked = np.where(ck == 1)[0]
            non_click = np.where(ck == 0)[0]
            for idx, item in enumerate(clicked):
                self.w[self.r_idx[item], self.r_idx[non_click]] += 1
        # else:
        #     zero_idx = [item == 0 for item in ck]
        #     for idx, item in enumerate(ck):
        #         if item == 1:
        #             beat_idx = np.where(zero_idx[:idx])[0]
        #             if len(beat_idx) == 0:
        #                 self.w[self.r_idx[idx], -1] += 1
        #             else:
        #                 self.w[self.r_idx[idx], self.r_idx[beat_idx]] += 1

    def get_stat(self):
        n = self.w.T + self.w
        score = self.policy(self.w.flat[:], n.flat[:], self.t, self.c).reshape(self.L + 1, self.L + 1)
        np.fill_diagonal(score, 0.5)
        copeland_score = (score > 0.5).sum(axis=1)
        return self.w.sum(axis=1) / n.sum(axis=1), copeland_score


