from abstract_rank import AbstractRank
import numpy as np
import logging


class DCMBandits(AbstractRank):
    def __init__(self, items, kappa, policy, c=3, T=1e5):
        """
        implementation of DCM paper
        :param L: size of candidata pool
        :param K: size of positions
        :param bound: bandit algorithm
        :param c: bandit parameter
        """
        super(DCMBandits, self).__init__(items, kappa, policy, c, T)

    def get_arms(self, l=-1):
        score = self.policy(self.w, self.n, self.t, self.c)
        aux_score = np.random.rand(self.L)
        idx = np.lexsort((aux_score, -score))
        self.r_idx = idx[:self.K]
        return self.r_idx

    def update(self, ck, arms=None):
        self.t += 1
        if np.sum(ck) == 0:
            self.n[self.r_idx] += 1
        else:
            c = np.where(ck == 1)[0]
            self.w[self.r_idx[c]] += 1
            self.n[self.r_idx[:c[-1]+1]] += 1
