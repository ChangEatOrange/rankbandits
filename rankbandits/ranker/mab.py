from utils import klucb, ucb, ts, arg_max
import numpy as np


class MAB(object):
    def __init__(self, k, bandit, c):
        self.k = k
        self.bandit = bandit
        self.c = c
        self.w = np.zeros(self.k)
        self.n = np.ones(self.k)
        self.t = 1
        self.winner = -1

    def get_arm(self):
        score = self.bandit(self.w, self.n, self.t, self.c)
        self.winner = arg_max(score)
        return self.winner

    def update(self, reward):
        self.t += 1
        self.n[self.winner] += 1
        if reward == 1:
            self.w[self.winner] += 1

