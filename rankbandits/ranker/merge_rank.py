import numpy as np
from _klucb import klucb as kb
from _kllcb import kllcb as kl


def klucb(w, n, t, c=3, precision=1e-6):
    return kb(w, n, t, c, precision)


def kllcb(w, n, t, c=3, precision=1e-6):
    return kl(w, n, t, c, precision)


class RealMergeRankAlgorithm(object):
    def __init__(self, items, kappa, policy=None, c=3, T=1e7):
        try:
            self.kappa = kappa
            self.items = items
            self.K = len(items)  # number of docs
            self.cutoff = len(items) # number of positions
            self.D = np.arange(self.K, dtype='int32')
            self.C = np.zeros(self.K, dtype='float64')
            self.N = np.zeros(self.K, dtype='float64')
            self.S = []
            self.t = 0
            self.T =T
            self.use_kl = True
            self.clear = True
            self.deltas = [0.5]
            self.ranking = None
            self.random_state = np.random

        except KeyError as e:
            raise ValueError('missing %s argument' % e)

    def getName(self):
        '''
        Returns the name of the algorithm.
        '''
        return ('RealMergeRank' + ('Zero' if self.clear else '')
                                + ('KL' if self.use_kl else '')
                                + ('[' + self.feedback.upper() + ']'))

    def get_arms(self):
        self.ranking = np.copy(self.D)
        if self.t < self.T:
            # Shuffle the documents in each bucket uniformly at random.
            for ds in np.array_split(self.ranking, self.S):
                self.random_state.shuffle(ds)
        # Return the top-`cutoff` documents.
        return self.ranking[:self.cutoff]

    def update(self, clicks, ranking=None):
        if ranking is None:
            ranking = self.ranking
        # Sets cutoff for full feedback.
        cutoff = self.cutoff
        
        # Sets cutoff for first click feedback.
        # if self.feedback == 'fc':
        #     crs = np.flatnonzero(clicks)
        #     if crs.size > 0:
        #         cutoff = crs[0] + 1
        #
        # # Sets cutoff for last click feedback.
        # elif self.feedback == 'lc':
        crs = np.flatnonzero(clicks)
        if crs.size > 0:
            cutoff = crs[-1] + 1
        
        # Update the click and view counters for the most
        # recent impression.
        for d, c in zip(ranking[:cutoff], clicks[:cutoff]):
            self.C[d] += c
            self.N[d] += 1

        # The positions of old + new splits.
        nextS = []

        # The new deltas.
        nextDeltas = []

        # Lower-bound on the minimum number of impressions for each document.
        logDT = np.log(self.K * self.T)

        for ds, s, delta in zip(np.array_split(self.D, self.S), np.r_[0, self.S], self.deltas):
            # We keep the old split positions.
            nextS.append(s)
            
            # If the group consists of only a single document or not all
            # documents in it were shown sufficiently often, than nothing
            # happens.
            
            if len(ds) == 1 or (self.N[ds] < logDT / delta**2).any():
                # Keep the old delta.
                nextDeltas.append(delta)
                continue
            
            # ========================================================
            # UpdateBatch 'sub-routine' starts here.
            # ========================================================

            reset_counters = False

            # Compute the lower and upper confidence bounds for each
            # document in the group.
            mus = self.C[ds] / self.N[ds]
            cnf = logDT / self.N[ds]
            if self.use_kl:  # use Kullback-Leibler bounds
                ucb = klucb(self.C[ds], self.N[ds], logDT)
                lcb = kllcb(self.C[ds], self.N[ds], logDT)
            else:  # use Chernoff-Hoeffding bounds
                cbs = np.sqrt(cnf)
                ucb = mus + cbs
                lcb = mus - cbs

            # Sort the documents within the group by
            # their lower-confidence bounds.
            sorter = np.argsort(lcb)[::-1]

            ds[:] = ds[sorter]
            ucb = ucb[sorter]
            lcb = lcb[sorter]

            # Try eliminate documents for the group spanning
            # over the cutoff position.
            if s + len(ds) > self.cutoff:
                k = self.cutoff - s - 1
                i = k + 1
                j = k + 1

                while j < len(ds):
                    if ucb[j] < lcb[k]:
                        reset_counters = True
                    else:
                        ds[i] = ds[j]
                        ucb[i] = ucb[j]
                        lcb[i] = lcb[j]
                        i += 1
                    j += 1
                    
                # if i != j:
                    # print 'Eliminating %d documents...' % (j - i)

                ds = ds[:i]
                ucb = ucb[:i]
                lcb = lcb[:i]

                self.D = self.D[:s + i]

            running_max_ucb = -np.ones_like(ucb)
            running_min_lcb =  np.ones_like(lcb)

            # Find the minimal LCB for consecutive documents
            # from top to bottom.
            running_min_lcb[0] = lcb[0]
            for i in range(1, len(ds)):
                running_min_lcb[i] = min(running_min_lcb[i - 1], lcb[i])
            
            # Find the maximal UCB for consecutive documents
            # from bottom to top.
            running_max_ucb[-1] = ucb[-1]
            for i in range(2, len(ds) + 1):
                running_max_ucb[-i] = max(running_max_ucb[-(i - 1)], ucb[-i])
            
            # See whether documents in the group cannot be
            # split into smaller groups.
            for i in range(1, len(ds)):
                if running_min_lcb[i - 1] > running_max_ucb[i]:
                    nextS.append(s + i)
                    nextDeltas.append(0.5)
                    reset_counters = True

            # Has the group of documents changed (elimination/split)?
            if reset_counters:
                # Should we clear the feedback after split?
                if self.clear:
                    # print 'Resetting counters...'
                    self.C[ds] = 0
                    self.N[ds] = 0

                # nextDeltas.append(delta / 2.0)
                nextDeltas.append(0.5)
            else:
                nextDeltas.append(delta / 2.0)            
            
        # Update the splits (omitting the 1st, which is
        # just an auxiliary index).
        self.S = np.array(nextS[1:], dtype='int32')

        # Update the deltas
        self.deltas = nextDeltas

        self.t += 1
       