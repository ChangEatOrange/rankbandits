from utils import ts, ucb, lcb
from _klucb import klucb as kb
from _kllcb import kllcb as kl
import numpy as np
import pickle


def klucb(w, n, t, c=3, precision=1e-5):
    return kb(w, n, t, c, precision)


def kllcb(w, n, t, c=3, precision=1e-5):
    return kl(w, n, t, c, precision)


POLICY_DICT = {'ts': ts, 'ucb': ucb, 'klucb': klucb}
LOWER_DICT = {'ts': ts, 'ucb': lcb, 'klucb': kllcb}


class AbstractRank(object):
    def __init__(self, items, kappa, policy=None, c=3, T=1e6):
        """ the abstract class
        :param items: items id
        :param kappa: probability of observing only used in PBM
        :param policy: ucb, klucb, ts
        :param c: bandit parameter
        """
        self.T = T
        self.L = len(items)
        self.K = len(kappa)
        self.items = items
        self.kappa = np.asarray(kappa)
        self.c = c
        self.policy = POLICY_DICT[policy]
        self.lower = LOWER_DICT[policy]
        self.w = np.zeros(self.L)
        self.n = np.ones(self.L)
        self.t = 1
        self.r_idx = []
        self.name = 'abstract'

    def get_arms(self, l):
        raise NotImplementedError

    def update(self, ck, arms):
        raise NotImplementedError

    def get_stat(self):
        return self.w/self.n, self.policy(self.w, self.n, self.t, self.c)

    def save(self, fname):
        with open(fname, 'wb') as f:
            pickle.dump(self, f)

    @classmethod
    def load(cls, fname):
        with open(fname, 'rb') as f:
            return pickle.load(f)
