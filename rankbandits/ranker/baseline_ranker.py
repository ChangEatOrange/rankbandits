import numpy as np
from abstract_rank import AbstractRank
import logging


class BaselineRanker(AbstractRank):
    def __init__(self, items, kappa, policy, c=3):
        """ implementation of Cascading bandits paper
        :param items: items id
        :param kappa: probability of observing only used in PBM
        :param policy: ucb, klucb, ts
        :param c: bandit parameter
        """
        super(BaselineRanker, self).__init__(items, kappa, policy, c)

    def get_arms(self, l=-1):
        if l == -1:
            l = self.L
        self.r_idx = self.items[:self.K]
        return self.r_idx

    def update(self, ck, arms=None):
        pass

