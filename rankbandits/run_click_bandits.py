from ranker import CascadingBandits
from ranker import MergeRank
from ranker import BubbleRank, BubbleRankKLUCB
from ranker import CombineRanker, CombineRankerFreq, CombineRankerPos
from ranker import BaselineRanker
from ranker import TopRank
from functools import partial
from user import PBMUser
from user import CascadeUser
from user import DCMUser

from random import shuffle

import multiprocessing as mp
import time
import datetime
import argparse
import timeit
import numpy as np
import os
import logging
import csv
import pickle

FLAGS = None
START_TIME = str(datetime.datetime.now()).replace(' ', '-').replace(':', '').split('.')[0]


METHOD_DICT = ['BubbleRank', 'MergeRank', 'CascadeKL-UCB', 'BaselineRank', 'TopRank']
METHOD_DICT = ['BubbleRank', 'MergeRank', 'TopRank']
METHOD_DICT = ['TopRank']

METHOD_INIT = {'CascadeKL-UCB': lambda l, kp, policy, c: CascadingBandits(l, kp, policy, c),
               'MergeRank': lambda l, kp, policy, c: MergeRank(l, kp, policy, c, FLAGS.iteration),
               'BubbleRank': lambda l, kp, policy, c: BubbleRank(l, kp, policy, c, FLAGS.iteration),
               'TopRank': lambda l, kp, policy, c: TopRank(l, kp, policy, c, FLAGS.iteration),
               'BaselineRank': lambda l, kp, policy, c: BaselineRanker(l, kp, policy, c),
               'CombineRanker': lambda l, kp, policy, c: CombineRanker(l, kp, policy, c),
               'CombineRankerFreq': lambda l, kp, policy, c: CombineRankerFreq(l, kp, policy, c),
               'CombineRankerFreq1e3': lambda l, kp, policy, c: CombineRankerFreq(l, kp, policy, c, initial_step=1000),
               'CombineRankerFreq1e4': lambda l, kp, policy, c: CombineRankerFreq(l, kp, policy, c, initial_step=10000),
               'CombineRankerPos': lambda l, kp, policy, c: CombineRankerPos(l, kp, policy, c),
               'CombineRankerPos1e4': lambda l, kp, policy, c: CombineRankerPos(l, kp, policy, c, initial_step=10000),
               'CombineRankerPos1e3': lambda l, kp, policy, c: CombineRankerPos(l, kp, policy, c, initial_step=1000),
               'CombineRankerPos1e2': lambda l, kp, policy, c: CombineRankerPos(l, kp, policy, c, initial_step=100),
               'BubbleRankKLUCB': lambda l, kp, policy, c: BubbleRankKLUCB(l, kp, policy, c),
               }

COLOR_DICT = ['blue', 'red', 'green', 'cyan', 'purple', 'brown', 'c', 'black']
POLICY_DICT = ['UCB', 'KLUCB', 'TS']
USER_DICT = [CascadeUser, PBMUser, DCMUser]
USER_NAME = ['CM', 'PBM', 'DCM']
FLAGS = []
SEED =[]


def get_data(dname):
    # pa and kappa
    kappa = [0.99997132, 0.95949374, 0.76096783, 0.59179909, 0.45740329, 0.38584302, 0.33052186, 0.28372475,
             0.26211924, 0.26700303]  # 60 queries
    kappa = [0.99999257, 0.96679847, 0.7973465, 0.63112651, 0.50237947, 0.41921298, 0.35512778, 0.30566137,
             0.28128806, 0.2852233]  # 100 queriees
    p_stop = [0.6555304, 0.4868164, 0.46051615, 0.46315161, 0.45642676, 0.47130397, 0.50317268, 0.54764235,
              0.65359742, 0.99998025]
    data = {'214456': [[0.35837245696400627, 0.2017167381974249, 0.1617161716171617, 0.08968609865470852, 0.08888888888888889, 0.08403361344537816, 0.07936507936507936, 0.07894736842105263, 0.06862745098039216, 0.0196078431372549],
                       [0.99997132, 0.95949374, 0.76096783, 0.59179909, 0.45740329, 0.38584302, 0.33052186, 0.28372475, 0.26211924, 0.26700303],
                       [1.0]*10],
            '214456-pbm': [
                [0.3664328634448365, 0.27362727716815927, 0.26777187725087265, 0.24413477103416745, 0.2374806060211961,
                 0.22481636110669356, 0.20030370998650943, 0.19428554641757073, 0.1883534631797853, 0.05628012942974181],
                [0.99997132, 0.95949374, 0.76096783, 0.59179909, 0.45740329, 0.38584302, 0.33052186, 0.28372475,
                 0.26211924, 0.26700303],
                [1.0] * 10],
            }
    f = open('../data/' + USER_NAME[FLAGS.user] + '.csv')
    reader = csv.reader(f)
    reader.next()
    for i in range(dname):
        data = reader.next()
    q_id = 'q'+data[0]
    p_data = [float(t) for t in data[1][1:-1].split(',')]
    # shuffle(p_data)
    return q_id, np.asarray(p_data), kappa, p_stop


def n_ctr(attr):
    counter = 0
    for i in range(len(attr)):
        for j in range(i, len(attr)):
            if attr[i] < attr[j]:
                counter += 1
    return counter


def save_results(cum_regret, ctr, data_name, policy, username, method, suffix, n_top):
    # if ctr != 1.0:
    #     ctr = np.floor(ctr * 10) / 10.

    dir_name = './results/' + str(FLAGS.iteration) + '/' + username + str(n_top) + '/' + policy + '/' + data_name + '/' + method
    if not os.path.exists(dir_name):
        os.makedirs(dir_name)
    save_name = dir_name + '/' + str(ctr) + '-' +suffix + '.pkl'
    f = open(save_name, 'wb')
    # np.save(f, cum_regret)
    pickle.dump(cum_regret, f)
    f.close()


def individual_run(counter, policy, c=3):
    """
    call the ranker
    :param counter: index of repeat
    :param policy:
    :param c:
    :param user: CM OR PBM
    :return: cumulative regret and running time
    """
    # time.sleep(max(counter, 10))
    # pa, kappa, p_stop = get_data(DATASET[FLAGS.data_idx])
    np.random.seed(SEED[counter])
    q_id, pa, kappa, p_stop = get_data(FLAGS.data_idx)
    nctr = n_ctr(pa)

    user = USER_DICT[FLAGS.user](pa, kappa, p_stop)
    suffix = 'rep-' + str(counter) + '-' + str(datetime.datetime.now()).replace(' ', '-').replace(':', '').split('.')[0]\
             + '-' + str(np.random.randint(1000))

    click_bandit = [METHOD_INIT[key](range(user.n_items), user.kappa[:], policy, c) for key in METHOD_DICT]

    # regrets10 = np.zeros((len(METHOD_DICT), FLAGS.iteration / FLAGS.scale))
    regrets5 = np.zeros((len(METHOD_DICT), FLAGS.iteration / FLAGS.scale))
    ndcg5_rel = np.zeros((len(METHOD_DICT), FLAGS.iteration / FLAGS.scale))
    ndcg5_power_rel = np.zeros((len(METHOD_DICT), FLAGS.iteration / FLAGS.scale))
    conservative = np.zeros((len(METHOD_DICT), FLAGS.iteration / FLAGS.scale))
    conservative_baseline = np.zeros((len(METHOD_DICT), FLAGS.iteration / FLAGS.scale))
    s = timeit.default_timer()
    for i in range(FLAGS.iteration):
        for ranker_idx in range(len(METHOD_DICT)):
            arms = click_bandit[ranker_idx].get_arms()
            ck = user.get_click(arms)
            click_bandit[ranker_idx].update(ck, arms)
            # regrets10[ranker_idx, i / FLAGS.scale] += user.regret(arms[:10])
            regrets5[ranker_idx, i / FLAGS.scale] += user.regret(arms[:5])
            ndcg5_power_rel[ranker_idx, i / FLAGS.scale] += user.ndcg_power_rel(arms[:5])
            ndcg5_rel[ranker_idx, i / FLAGS.scale] += user.ndcg_rel(arms[:5])
            conservative[ranker_idx, i / FLAGS.scale] += user.is_valid(arms)
            conservative_baseline[ranker_idx, i / FLAGS.scale] += user.is_valid_to_baselien(arms)
    logging.info('Iteration: %d finished' % counter)
    time_run = timeit.default_timer() - s
    # cum_regret = regrets10.cumsum(axis=1)
    # [save_results(cum_regret[i], n_ctr(user.attraction), q_id, POLICY_DICT[FLAGS.policy], USER_NAME[FLAGS.user],
    # METHOD_DICT[i], suffix, 10) for i in range(len(METHOD_DICT))]

    cum_regret = regrets5.cumsum(axis=1)
    [save_results([cum_regret[i], ndcg5_rel[i]/FLAGS.scale, ndcg5_power_rel[i]/FLAGS.scale, conservative[i],
                   conservative_baseline[i]], n_ctr(user.attraction), q_id, POLICY_DICT[FLAGS.policy],
                  USER_NAME[FLAGS.user], METHOD_DICT[i], suffix, 5) for i in
     range(len(METHOD_DICT))]
    return time_run


if __name__ == '__main__':
    logging.basicConfig(format='%(asctime)s : %(message)s', level=logging.INFO)
    parser = argparse.ArgumentParser()
    parser.add_argument('--n_jobs', default=2, type=int)
    parser.add_argument('--data_idx', default=2, type=int)
    parser.add_argument('--policy', default=1, type=int)
    parser.add_argument('--iteration', default=int(5e6), type=int)
    parser.add_argument('--repeat', default=10, type=int)
    parser.add_argument('--scale', default=10, type=int)
    parser.add_argument('--user', default=0, type=int)
    parser.add_argument('--n_top', default=5, type=int)
    parser.add_argument('--avg_bins', default=1, type=int)
    FLAGS = parser.parse_args()
    time.sleep(np.random.randint(10))
    
    for key, value in vars(FLAGS).items():
        logging.info(key + ' : ' + str(value))

    # make dir
    q_id, pa, kappa, p_stop = get_data(FLAGS.data_idx)
    for username in USER_NAME:
        n_top = 5
        policy = 'KLUCB'
        for method in METHOD_DICT:
            dir_name = './results/' + str(FLAGS.iteration) + '/' + username + str(n_top) + '/' + policy + '/' + q_id + '/' + method
            if not os.path.exists(dir_name):
                os.makedirs(dir_name)
    # np.random.seed(42)
    SEED = np.random.choice(10000, FLAGS.repeat)
    pool = mp.Pool(FLAGS.n_jobs)
    for idx, item in enumerate(pool.imap(partial(individual_run, policy=POLICY_DICT[FLAGS.policy].lower()), range(FLAGS.repeat))):
        logging.info('Repeat %d has been finished! With running time%f' % (idx, item))
    # for idx in range(FLAGS.repeat):
    #     item = individual_run(idx, policy=POLICY_DICT[FLAGS.policy].lower())
    #     logging.info('Repeat %d has been finished! With running time%f' % (idx, item))


