import numpy as np


def kl_bern(p, q):
    """
    kl divergence between two bernoulli distributions
    :param p:
    :param q:
    :return:
    """
    eps = 1e-15
    p = min(max(p, eps), 1-eps)
    q = min(max(q, eps), 1-eps)
    return p * np.log(p/q) + (1-p) * np.log((1-p)/(1-q))


def ucb(w, n, t, alpha):
    return (w + 0.0)/n + np.sqrt(alpha * np.log(t)/n)


def lcb(w, n, t, alpha):
    return (w + 0.0) / n - np.sqrt(alpha * np.log(t) / n)


def klucb_individual(w, n, t, c, precision=1e-6):
    if np.e ** 2 > t:
        return 1.0 / n
    u = min(1., ucb(w, n, t, 0.51))
    l = min(w / n, 1 - precision)
    p = w / n
    delta = (np.log(t) + c * np.log(np.log(t))) / n
    while (u - l) > precision:
        q = (u + l) / 2.0
        if kl_bern(p, q) > delta:
            u = q
        else:
            l = q
    return (u+l)/2.0


def klucb(w2, n, t, c, precision=1e-5):
    w = w2 + 0.0
    klu = np.vectorize(klucb_individual)
    return klu(w, n, t, c, precision)


def ts(w, n, t, c):
    return np.random.beta(w + 1, 1 + n - w)


def get_click(cp, kappa, positions):
    cp = np.asarray(cp)
    k = len(kappa)
    coins = np.random.rand(k)
    click_prob = np.multiply(cp[positions], kappa)
    return coins < click_prob


def arg_max(array):
    max_element = np.max(array)
    winners = np.where(array == max_element)[0]
    return winners[np.random.randint(len(winners))]


def arg_min(array):
    min_element = np.min(array)
    winners = np.where(array == min_element)[0]
    return winners[np.random.randint(len(winners))]

