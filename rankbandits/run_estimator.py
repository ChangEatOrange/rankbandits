from ranker import BubbleRank
from functools import partial
from user import CascadeUser, PBMUser
from random import shuffle

import multiprocessing as mp
import time
import datetime
import argparse
import timeit
import numpy as np
import os
import logging
import csv

FLAGS = None
START_TIME = str(datetime.datetime.now()).replace(' ', '-').replace(':', '').split('.')[0]

METHOD_DICT = ['BubbleRank']

METHOD_INIT = {'BubbleRank': lambda l, kp, policy, c: BubbleRank(l, kp, policy, c)}

COLOR_DICT = ['blue', 'red', 'green', 'cyan', 'purple', 'brown', 'c', 'black']
POLICY_DICT = ['UCB', 'KLUCB', 'TS']
USER_DICT = [PBMUser]
USER_NAME = ['PBM']
FLAGS = []


def get_data(dname):
    # pa and kappa
    kappa = [0.9] * 8+ [pow(0.5, dname)] *2
    p_stop = [1.0]*10
    p_data = [0.5] * 9 + [0.9]
    return dname, np.asarray(p_data), kappa, p_stop


def n_ctr(attr):
    counter = 0
    for i in range(len(attr)):
        for j in range(i, len(attr)):
            if attr[i] < attr[j]:
                counter += 1
    return counter


def save_results(cum_regret, ctr, data_name, policy, username, method, suffix, n_top):
    # if ctr != 1.0:
    #     ctr = np.floor(ctr * 10) / 10.

    dir_name = './results/estimator/' + str(FLAGS.iteration) + '/' + username + str(n_top) + '/' + policy + '/' + str(data_name) + '/' + method
    if not os.path.exists(dir_name):
        os.makedirs(dir_name)
    save_name = dir_name + '/' + str(ctr) + '-' +suffix + '.npy'
    f = open(save_name, 'wb')
    np.save(f, cum_regret)
    f.close()


def individual_run(counter, policy, c=3):
    """
    call the ranker
    :param counter: index of repeat
    :param policy:
    :param c:
    :param user: CM OR PBM
    :return: cumulative regret and running time
    """
    # time.sleep(max(counter, 10))
    # pa, kappa, p_stop = get_data(DATASET[FLAGS.data_idx])
    q_id, pa, kappa, p_stop = get_data(FLAGS.data_idx)
    nctr = n_ctr(pa)
    user = USER_DICT[FLAGS.user](pa, kappa, p_stop)
    suffix = 'rep-' + str(counter) + '-' + str(datetime.datetime.now()).replace(' ', '-').replace(':', '').split('.')[0]\
             + '-' + str(np.random.randint(1000))

    click_bandit = [METHOD_INIT['BubbleRank'](range(user.n_items), user.kappa[:], policy, c)]

    # regrets3 = np.zeros((len(METHOD_DICT), FLAGS.iteration / FLAGS.scale))
    regrets5 = np.zeros((len(METHOD_DICT), FLAGS.iteration / FLAGS.scale))
    s = timeit.default_timer()
    for i in range(FLAGS.iteration):
        for ranker_idx in range(len(METHOD_DICT)):
            arms = click_bandit[ranker_idx].get_arms()
            ck = user.get_click(arms)
            click_bandit[ranker_idx].update(ck, arms)
            # regrets3[ranker_idx, i / FLAGS.scale] += user.regret(arms[:3])
            regrets5[ranker_idx, i / FLAGS.scale] += user.regret(arms[:5])
    logging.info('Iteration: %d finished' % counter)
    time_run = timeit.default_timer() - s
    # cum_regret = regrets3.cumsum(axis=1)
    # [save_results(cum_regret[i], n_ctr(user.attraction), q_id, POLICY_DICT[FLAGS.policy], USER_NAME[FLAGS.user], METHOD_DICT[i], suffix, 3) for i in range(len(METHOD_DICT))]
    cum_regret = regrets5.cumsum(axis=1)
    [save_results(cum_regret[i], n_ctr(user.attraction), q_id, POLICY_DICT[FLAGS.policy], USER_NAME[FLAGS.user], METHOD_DICT[i], suffix, 5) for i in range(len(METHOD_DICT))]
    return time_run

if __name__ == '__main__':
    logging.basicConfig(format='%(asctime)s : %(message)s', level=logging.INFO)
    parser = argparse.ArgumentParser()
    parser.add_argument('--n_jobs', default=4, type=int)
    parser.add_argument('--data_idx', default=0, type=int)
    parser.add_argument('--policy', default=1, type=int)
    parser.add_argument('--iteration', default=int(1e6), type=int)
    parser.add_argument('--repeat', default=8, type=int)
    parser.add_argument('--scale', default=1000, type=int)
    parser.add_argument('--user', default=0, type=int)
    parser.add_argument('--n_top', default=5, type=int)
    parser.add_argument('--avg_bins', default=1, type=int)
    FLAGS = parser.parse_args()
    time.sleep(np.random.randint(10))
    
    for key, value in vars(FLAGS).items():
        logging.info(key + ' : ' + str(value))

    # make dir
    q_id, pa, kappa, p_stop = get_data(FLAGS.data_idx)
    for username in USER_NAME:
        n_top = 5
        policy = 'KLUCB'
        for method in METHOD_DICT:
            dir_name = './results/estimator/' + str(FLAGS.iteration) + '/' + username + str(n_top) + '/' + policy + '/' + str(q_id) + '/' + method
            if not os.path.exists(dir_name):
                os.makedirs(dir_name)

    pool = mp.Pool(FLAGS.n_jobs)
    for idx, item in enumerate(pool.imap(partial(individual_run, policy=POLICY_DICT[FLAGS.policy].lower()), range(FLAGS.repeat))):
        logging.info('Repeat %d has been finished! With running time%f' % (idx, item))
    # for idx in range(FLAGS.repeat):
    #     item = individual_run(idx, policy=POLICY_DICT[FLAGS.policy].lower())
    #     logging.info('Repeat %d has been finished! With running time%f' % (idx, item))


