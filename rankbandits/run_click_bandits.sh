#!/bin/sh

#SBATCH --mem=2G
#SBATCH -c2
#SBATCH -o ./out/%A_%a.out
#SBATCH -e ./out/%A_%a.err
#SBATCH -p cpu,mem
#SBATCH --time=8:00:00
#SBATCH --array=1-100%34

cd /zfs/ilps-plex1/slurm/datastore/cli1
source ./anaconda/bin/activate bandit_venv

cd /zfs/ilps-plex1/slurm/datastore/cli1/rankbandits/rankbandits

# if [ $# = 2 ]
#then
#	python run_click_bandits.py --data_idx=$1 --user=$2 --iteration=1000 --scale=1 --repeat=1000
#else
#python run_click_bandits.py --data_idx=$1 --user=$2
#fi

# touch hparams-yandex.txt
# for i in {1..100}; do echo "--data_idx=$i" >> hparams-yandex.txt; done

HPARAMS_FILE=hparams-yandex.txt

python run_click_bandits.py --user=$1 $(head -$SLURM_ARRAY_TASK_ID $HPARAMS_FILE | tail -1)


source deactivate
