from ranker import CascadingBandits
from ranker import MergeRank
from ranker import BubbleRank, BubbleRankKLUCB
from ranker import CombineRanker, CombineRankerFreq, CombineRankerPos
from ranker import BaselineRanker
from functools import partial
from user import PBMUser
from user import CascadeUser
from user import DCMUser


import multiprocessing as mp
import time
import datetime
import argparse
import timeit
import numpy as np
import os
import logging

FLAGS = None
START_TIME = str(datetime.datetime.now()).replace(' ', '-').replace(':', '').split('.')[0]

METHOD_DICT = ['MergeRank', 'CascadeKL-UCB', 'BubbleRank', 'BaselineRank']

METHOD_INIT = {'CascadeKL-UCB': lambda l, kp, policy, c: CascadingBandits(l, kp, policy, c),
               'MergeRank': lambda l, kp, policy, c: MergeRank(l, kp, policy, c),
               'BaselineRank': lambda l, kp, policy, c: BaselineRanker(l, kp, policy, c),
               'BubbleRank': lambda l, kp, policy, c: BubbleRank(l, kp, policy, c),
               'CombineRanker': lambda l, kp, policy, c: CombineRanker(l, kp, policy, c),
               'CombineRankerFreq': lambda l, kp, policy, c: CombineRankerFreq(l, kp, policy, c),
               'CombineRankerFreq1e3': lambda l, kp, policy, c: CombineRankerFreq(l, kp, policy, c, initial_step=1000),
               'CombineRankerFreq1e4': lambda l, kp, policy, c: CombineRankerFreq(l, kp, policy, c, initial_step=10000),
               'CombineRankerPos': lambda l, kp, policy, c: CombineRankerPos(l, kp, policy, c),
               'CombineRankerPos1e4': lambda l, kp, policy, c: CombineRankerPos(l, kp, policy, c, initial_step=10000),
               'CombineRankerPos1e3': lambda l, kp, policy, c: CombineRankerPos(l, kp, policy, c, initial_step=1000),
               'CombineRankerPos1e2': lambda l, kp, policy, c: CombineRankerPos(l, kp, policy, c, initial_step=100),
               'BubbleRankKLUCB': lambda l, kp, policy, c: BubbleRankKLUCB(l, kp, policy, c)}

POLICY_DICT = ['UCB', 'KLUCB', 'TS']
USER_DICT = [CascadeUser, PBMUser, DCMUser]
DATASET = ['Yahoo', 'MSLR']
USER_NAME = ['CM', 'PBM', 'DCM']
FLAGS = []
SEED = []
CONFIGURATION = np.asarray([0.0272, 0.0875, 0.1518, 0.2328, 0.4165])
CONFIGURATION = np.asarray([0.0106, 0.027, 0.0584, 0.1164, 0.3548])
CONFIGURATION = np.asarray([0.05, 0.3, 0.5, 0.7, 0.95])


def get_data(dname):
    # pa and kappa
    kappa = [0.99997132, 0.95949374, 0.76096783, 0.59179909, 0.45740329, 0.38584302, 0.33052186, 0.28372475,
             0.26211924, 0.26700303] # 60 queries
    kappa = [0.99999257, 0.96679847, 0.7973465, 0.63112651, 0.50237947, 0.41921298, 0.35512778, 0.30566137,
             0.28128806, 0.2852233] # 100 queriees
    p_stop = [0.6555304, 0.4868164, 0.46051615, 0.46315161, 0.45642676, 0.47130397, 0.50317268, 0.54764235,
              0.65359742, 0.99998025]

    f = open('../data/' + DATASET[FLAGS.dataset_idx] + '.txt')
    for i, fitem in enumerate(f):
        if i == dname:
            item = fitem.replace('|', '').replace('[', '').replace(']', '')
            s = item.split(' ')
            q_id = 'q'+s[1]
            p_data = [CONFIGURATION[int(t)] for t in s[2:]]
            break

    return q_id, np.asarray(p_data) + np.random.rand(len(p_data))*1e-10, kappa, p_stop


def n_ctr(attr):
    counter = 0
    for i in range(len(attr)):
        for j in range(i, len(attr)):
            if attr[i] < attr[j]:
                counter += 1
    return counter


def save_results(cum_regret, ctr, data_name, policy, username, method, suffix, n_top):
    dir_name = './results/' + str(FLAGS.iteration) + '/' + username + str(
        n_top) + '/' + policy + '/' + data_name + '/' + method
    if not os.path.exists(dir_name):
        try: 
            os.makedirs(dir_name)
        except:
            logging.info('file exist.')
    save_name = dir_name + '/' + str(ctr) + '-' + suffix + '.npy'
    f = open(save_name, 'wb')
    np.save(f, cum_regret)
    f.close()


def individual_run(counter, policy, c=3):
    """
    call the ranker
    :param counter: index of repeat
    :param policy:
    :param c:
    :param user: CM OR PBM
    :return: cumulative regret and running time
    """
    # time.sleep(max(counter, 10))
    # pa, kappa, p_stop = get_data(DATASET[FLAGS.data_idx])
    np.random.seed(SEED[counter])
    q_id, pa, kappa, p_stop = get_data(FLAGS.data_idx)
    nctr = n_ctr(pa)

    user = USER_DICT[FLAGS.user](pa, kappa, p_stop)
    suffix = 'rep-' + str(counter) + '-' + str(datetime.datetime.now()).replace(' ', '-').replace(':', '').split('.')[0] \
             + '-' + str(np.random.randint(1000))

    click_bandit = [METHOD_INIT[key](range(user.n_items), user.kappa[:], policy, c) for key in METHOD_DICT]

    regrets3 = np.zeros((len(METHOD_DICT), FLAGS.iteration / FLAGS.scale))
    regrets5 = np.zeros((len(METHOD_DICT), FLAGS.iteration / FLAGS.scale))
    s = timeit.default_timer()
    for i in range(FLAGS.iteration):
        for ranker_idx in range(len(METHOD_DICT)):
            arms = click_bandit[ranker_idx].get_arms()
            ck = user.get_click(arms)
            click_bandit[ranker_idx].update(ck, arms)
            regrets3[ranker_idx, i / FLAGS.scale] += user.regret(arms[:3])
            regrets5[ranker_idx, i / FLAGS.scale] += user.regret(arms[:5])
    logging.info('Iteration: %d finished' % counter)
    time_run = timeit.default_timer() - s
    # top 3
    cum_regret = regrets3.cumsum(axis=1)
    [save_results(cum_regret[i], n_ctr(user.attraction), q_id, POLICY_DICT[FLAGS.policy],
                  DATASET[FLAGS.dataset_idx]+USER_NAME[FLAGS.user], METHOD_DICT[i], suffix, 3) for i in range(len(METHOD_DICT))]
    # top 5
    cum_regret = regrets5.cumsum(axis=1)
    [save_results(cum_regret[i], n_ctr(user.attraction), q_id, POLICY_DICT[FLAGS.policy],
                  DATASET[FLAGS.dataset_idx]+USER_NAME[FLAGS.user], METHOD_DICT[i], suffix, 5) for i in range(len(METHOD_DICT))]
    return time_run


if __name__ == '__main__':
    logging.basicConfig(format='%(asctime)s : %(message)s', level=logging.INFO)
    parser = argparse.ArgumentParser()
    parser.add_argument('--n_jobs', default=2, type=int)
    parser.add_argument('--dataset_idx', default=0, type=int, help='0 for Yahoo, 1 for MSLR')
    parser.add_argument('--data_idx', default=29, type=int)
    parser.add_argument('--policy', default=1, type=int)
    parser.add_argument('--iteration', default=int(1e5) + 1, type=int)
    parser.add_argument('--repeat', default=2, type=int)
    parser.add_argument('--scale', default=1, type=int)
    parser.add_argument('--user', default=0, type=int)
    parser.add_argument('--n_top', default=5, type=int)
    parser.add_argument('--avg_bins', default=1, type=int)
    parser.add_argument('--interloop', default=0, type=int)
    FLAGS = parser.parse_args()
    time.sleep(np.random.randint(10))
    FLAGS.iteration += 1000*FLAGS.interloop    

    for key, value in vars(FLAGS).items():
        logging.info(key + ' : ' + str(value))

    # make dir
    q_id, pa, kappa, p_stop = get_data(FLAGS.data_idx)
    # for data_name in DATASET:
    #     n_top = 5
    #     policy = 'KLUCB'
    #     for method in METHOD_DICT:
    #         dir_name = './results/' + str(FLAGS.iteration) + '/' + data_name + USER_NAME[FLAGS.user] + str(
    #             n_top) + '/' + policy + '/' + q_id + '/' + method
    #         if not os.path.exists(dir_name):
    #             os.makedirs(dir_name)
    # np.random.seed(42)
    SEED = np.random.choice(10000, FLAGS.repeat)
    pool = mp.Pool(FLAGS.n_jobs)
    for idx, item in enumerate(
            pool.imap(partial(individual_run, policy=POLICY_DICT[FLAGS.policy].lower()), range(FLAGS.repeat))):
        logging.info('Repeat %d has been finished! With running time%f' % (idx, item))
    # for idx in range(FLAGS.repeat):
    #     item = individual_run(idx, policy=POLICY_DICT[FLAGS.policy].lower())
    #     logging.info('Repeat %d has been finished! With running time%f' % (idx, item))


