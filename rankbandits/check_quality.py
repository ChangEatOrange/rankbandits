from ranker import CascadingBandits
from ranker import BatchRank
from ranker import  BubbleRank
from functools import partial
from user import PBMUser
from user import CascadeUser
from random import shuffle

import multiprocessing as mp
import time
import datetime
import argparse
import numpy as np
import os
import logging
import pickle as pk
import csv

FLAGS = None
START_TIME = str(datetime.datetime.now()).replace(' ', '-').replace(':', '').split('.')[0]

METHOD_DICT = ['BubbleRank', 'BatchRank', 'CascadeKL-UCB']
METHOD_DICT = ['CascadeKL-UCB']

METHOD_INIT = {'CascadeKL-UCB': lambda l, kp, policy, c: CascadingBandits(l, kp, policy, c),
               'BatchRank': lambda l, kp, policy, c: BatchRank(l, kp, policy, c),
               'BubbleRank': lambda l, kp, policy, c: BubbleRank(l, kp, policy, c)}


DATASET = ['214456', '214456-pbm']
COLOR_DICT = ['blue', 'red', 'green', 'cyan', 'purple', 'brown', 'c', 'black']
POLICY_DICT = ['UCB', 'KLUCB', 'TS']
USER_DICT = [CascadeUser, PBMUser]
USER_NAME = ['CM', 'PBM']
FLAGS = []

def get_data(dname):
    # pa and kappa
    kappa = [0.99997132, 0.95949374, 0.76096783, 0.59179909, 0.45740329, 0.38584302, 0.33052186, 0.28372475,
                 0.26211924, 0.26700303]
    p_stop = [1.0]*10
    data = {'214456': [[0.35837245696400627, 0.2017167381974249, 0.1617161716171617, 0.08968609865470852, 0.08888888888888889, 0.08403361344537816, 0.07936507936507936, 0.07894736842105263, 0.06862745098039216, 0.0196078431372549],
                       [0.99997132, 0.95949374, 0.76096783, 0.59179909, 0.45740329, 0.38584302, 0.33052186, 0.28372475, 0.26211924, 0.26700303],
                       [1.0]*10],
            '214456-pbm': [
                [0.3664328634448365, 0.27362727716815927, 0.26777187725087265, 0.24413477103416745, 0.2374806060211961,
                 0.22481636110669356, 0.20030370998650943, 0.19428554641757073, 0.1883534631797853, 0.05628012942974181],
                [0.99997132, 0.95949374, 0.76096783, 0.59179909, 0.45740329, 0.38584302, 0.33052186, 0.28372475,
                 0.26211924, 0.26700303],
                [1.0] * 10],
            }
    f = open('../data/PBM.csv')
    reader = csv.reader(f)
    reader.next()
    for i in range(dname):
        data = reader.next()
    q_id = 'q'+data[0]
    p_data = [float(t) for t in data[1][1:-1].split(',')]
    shuffle(p_data)
    return q_id, np.asarray(p_data), kappa, p_stop



def n_ctr(attr, pos, model='PBM'):
    if model == 'CM':
        ideal_att = np.sort(attr)[::-1]
        return (1 - np.prod(1 - attr[:pos])) / (1 - np.prod(1 - ideal_att[:pos]))
    if model == 'PBM':
        kappa = [0.99997132, 0.95949374, 0.76096783, 0.59179909, 0.45740329, 0.38584302, 0.33052186, 0.28372475,
                 0.26211924, 0.26700303]
        ideal_att = np.sort(attr)[::-1]
        return np.dot(attr[:pos], kappa[:pos]) / np.dot(ideal_att[:pos], kappa[:pos])



def save_results(results, data_name, policy, username, method):
    # if ctr != 1.0:
    #     ctr = np.floor(ctr * 10) / 10.

    dir_name = './results/quality/' + username + '/' + policy + '/' + data_name + '/'
    if not os.path.exists(dir_name):
        os.makedirs(dir_name)
    save_name = dir_name + method + '.pk'
    f = open(save_name, 'wb')
    pk.dump(results, f)
    f.close()

def individual_run(counter, policy, c=3):
    """
    call the ranker
    :param counter: index of repeat
    :param policy:
    :param c:
    :param user: CM OR PBM
    :return: cumulative regret and running time
    """
    # time.sleep(max(counter, 10))
    # pa, kappa, p_stop = get_data(DATASET[FLAGS.data_idx])
    q_id, pa, kappa, p_stop = get_data(FLAGS.data_idx)
    user = USER_DICT[FLAGS.user](pa, kappa, p_stop)

    click_bandit = [METHOD_INIT[key](range(user.n_items), user.kappa[:], policy, c) for key in METHOD_DICT]
    logging.info('Initialy ranking quality: %f' % n_ctr(user.attraction, FLAGS.n_top, USER_NAME[FLAGS.user]))

    quality3 = []
    quality5 = []
    regret3 = []
    regret5 = []

    for i in range(FLAGS.iteration):
        for ranker_idx in range(len(METHOD_DICT)):
            arms = click_bandit[ranker_idx].get_arms()
            ck = user.get_click(arms)
            click_bandit[ranker_idx].update(ck, arms)
            if (i+1) % 20 ==0 and (i+1) < 201 or (i+1) == FLAGS.iteration:
                qt3 = 0.
                rt3 = 0.
                qt5 = 0.
                rt5 = 0.
                for ii in range(100):
                    arms = click_bandit[ranker_idx].get_arms()
                    p_click = user.attraction[arms]
                    qt3 += n_ctr(p_click, 3, USER_NAME[FLAGS.user])
                    rt3 += user.regret(arms[:3])
                    qt5 += n_ctr(p_click, 5, USER_NAME[FLAGS.user])
                    rt5 += user.regret(arms[:5])
                print "The quality and regret of the model (top 3) after %d time-steps are %f and %f" % (i+1, qt3/100, rt3/100)
                print "The quality and regret of the model (top 5) after %d time-steps are %f and %f" % (i+1, qt5/100, rt5/100)
                quality3.append(qt3/100)
                quality5.append(qt5/100)
                regret3.append(rt3/100)
                regret5.append(rt5/100)
    logging.info('Iteration: %d finished' % counter)

    return [quality3, quality5, regret3, regret5]

SAVE_DICT = ['n_ctr3', 'n_ctr5', 'regret3', 'regret5']
if __name__ == '__main__':
    logging.basicConfig(format='%(asctime)s : %(message)s', level=logging.INFO)
    parser = argparse.ArgumentParser()
    parser.add_argument('--n_jobs', default=3, type=int)
    parser.add_argument('--data_idx', default=1, type=int)
    parser.add_argument('--policy', default=1, type=int)
    parser.add_argument('--iteration', default=1000, type=int)
    parser.add_argument('--repeat', default=30, type=int)
    parser.add_argument('--scale', default=1000, type=int)
    parser.add_argument('--user', default=0, type=int)
    parser.add_argument('--n_top', default=3, type=int)
    FLAGS = parser.parse_args()
    time.sleep(np.random.randint(10))
    logging.info('Checki    ng the quality of Click Bandits under %s!' % USER_NAME[FLAGS.user])
    pool = mp.Pool(FLAGS.n_jobs)
    results = {'n_ctr3':[], 'n_ctr5': [], 'regret3': [], 'regret5': []}
    q_id, pa, kappa, p_stop = get_data(FLAGS.data_idx)
    for idx, item in enumerate(pool.imap(partial(individual_run, policy=POLICY_DICT[FLAGS.policy].lower()), range(FLAGS.repeat))):
        logging.info('Repeat %d has been finished!' % idx)
        [results[key].append(item[i]) for i, key in enumerate(SAVE_DICT)]

        # for idx in range(FLAGS.repeat):
    #     item = individual_run(idx, policy=POLICY_DICT[FLAGS.policy].lower())
    #     logging.info('Repeat %d has been finished!' % idx)
    #     [results[key].append(item[i]) for i, key in enumerate(results.keys())]

    save_results(results, q_id, POLICY_DICT[FLAGS.policy], USER_NAME[FLAGS.user], METHOD_DICT[0])

