from ranker import CascadingBandits
from ranker import PBMBandits
from ranker import BatchRank
from ranker import  BubbleRank, BubbleRankKLUCB
from functools import partial
from user import PBMUser
from user import CascadeUser
from random import shuffle

import multiprocessing as mp
import time
import datetime
import argparse
import timeit
import numpy as np
import os
import logging
import csv
import pickle as pk

FLAGS = None
START_TIME = str(datetime.datetime.now()).replace(' ', '-').replace(':', '').split('.')[0]

METHOD_DICT = ['BubbleRank']

METHOD_INIT = {'CascadeKL-UCB': lambda l, kp, policy, c: CascadingBandits(l, kp, policy, c),
               'BatchRank': lambda l, kp, policy, c: BatchRank(l, kp, policy, c),
               'BubbleRank': lambda l, kp, policy, c: BubbleRank(l, kp, policy, c),
               'BubbleRankKLUCB': lambda l, kp, policy, c: BubbleRankKLUCB(l, kp, policy, c)}

POLICY_DICT = ['UCB', 'KLUCB', 'TS']
USER_DICT = [CascadeUser, PBMUser]
USER_NAME = ['CM', 'PBM']
FLAGS = []


def get_data(dname):
    # pa and kappa
    kappa = [0.99997132, 0.95949374, 0.76096783, 0.59179909, 0.45740329, 0.38584302, 0.33052186, 0.28372475,
                 0.26211924, 0.26700303]
    p_stop = [1.0]*10
    f = open('../data/PBM.csv')
    reader = csv.reader(f)
    reader.next()
    for i in range(dname):
        data = reader.next()
    q_id = 'q'+data[0]
    p_data = [float(t) for t in data[1][1:-1].split(',')]
    shuffle(p_data)
    return q_id, np.asarray(p_data), kappa, p_stop


def n_ctr(attr, pos, model='PBM'):
    if model == 'CM':
        ideal_att = np.sort(attr)[::-1]
        return (1 - np.prod(1 - attr[:pos])) / (1 - np.prod(1 - ideal_att[:pos]))
    if model == 'PBM':
        kappa = [0.99997132, 0.95949374, 0.76096783, 0.59179909, 0.45740329, 0.38584302, 0.33052186, 0.28372475,
                 0.26211924, 0.26700303]
        ideal_att = np.sort(attr)[::-1]
        return np.dot(attr[:pos], kappa[:pos]) / np.dot(ideal_att[:pos], kappa[:pos])


def save_results(cum_regret, ctr, data_name, policy, username, method, suffix, n_top):
    # if ctr != 1.0:
    #     ctr = np.floor(ctr * 10) / 10.

    dir_name = './results/swap/' + username + str(n_top) + '/' + policy + '/' + data_name + '/' + method
    if not os.path.exists(dir_name):
        os.makedirs(dir_name)
    save_name = dir_name + '/' + str(ctr) + '-' +suffix + '.pk'
    f = open(save_name, 'wb')
    pk.dump(cum_regret, f)
    f.close()


def individual_run(counter, policy, c=3):
    """
    call the ranker
    :param counter: index of repeat
    :param policy:
    :param c:
    :param user: CM OR PBM
    :return: cumulative regret and running time
    """
    q_id, pa, kappa, p_stop = get_data(FLAGS.data_idx)

    user = USER_DICT[FLAGS.user](pa, kappa, p_stop)
    suffix = 'rep-' + str(counter) + '-' + str(datetime.datetime.now()).replace(' ', '-').replace(':', '').split('.')[0]\
             + '-' + str(np.random.randint(1000))

    click_bandit = [METHOD_INIT[key](range(user.n_items), user.kappa[:], policy, c) for key in METHOD_DICT]

    regrets3 = np.zeros(FLAGS.iteration / FLAGS.scale)
    regrets5 = np.zeros(FLAGS.iteration / FLAGS.scale)
    s = timeit.default_timer()
    last_swap = 0
    swap_counter = 0
    swap_results = []
    for i in range(FLAGS.iteration):
        ranker_idx = 0 
        arms = click_bandit[ranker_idx].get_arms()
        ck = user.get_click(arms)
        is_swap, begin = click_bandit[ranker_idx].update(ck, arms)
        regrets3[i / FLAGS.scale] += user.regret(arms[:3])
        regrets5[i / FLAGS.scale] += user.regret(arms[:5])
        if is_swap:
            swap_counter += 1
            swap_results.append([swap_counter, begin, last_swap, np.mean(regrets3[last_swap:i]), np.mean(regrets5[last_swap:i])])
            print swap_results[-1]
            last_swap = i
    save_results(swap_results, n_ctr(user.attraction, 3, USER_NAME[FLAGS.user]),
                 q_id, POLICY_DICT[FLAGS.policy], USER_NAME[FLAGS.user], METHOD_DICT[0], suffix, 3)
    logging.info('Iteration: %d finished' % counter)
    time_run = timeit.default_timer() - s
    return time_run


if __name__ == '__main__':
    """
    The regret change of BubbleRank after a swap. 
    """
    logging.basicConfig(format='%(asctime)s : %(message)s', level=logging.INFO)
    parser = argparse.ArgumentParser()
    parser.add_argument('--n_jobs', default=4, type=int)
    parser.add_argument('--data_idx', default=1, type=int)
    parser.add_argument('--policy', default=1, type=int)
    parser.add_argument('--iteration', default=1000000, type=int)
    parser.add_argument('--repeat', default=10, type=int)
    parser.add_argument('--scale', default=1, type=int)
    parser.add_argument('--user', default=0, type=int)
    parser.add_argument('--n_top', default=5, type=int)
    FLAGS = parser.parse_args()
    time.sleep(np.random.randint(10))
    pool = mp.Pool(FLAGS.n_jobs)
    for idx, item in enumerate(pool.imap(partial(individual_run, policy=POLICY_DICT[FLAGS.policy].lower()), range(FLAGS.repeat))):
        logging.info('Repeat %d has been finished! With running time%f' % (idx, item))
    # for idx in range(FLAGS.repeat):
    #     item = individual_run(idx, policy=POLICY_DICT[FLAGS.policy].lower())
    #     logging.info('Repeat %d has been finished! With running time%f' % (idx, item))


