import numpy as np
import os
import matplotlib
import pickle 

matplotlib.rcParams["axes.linewidth"] = 0.75
matplotlib.rcParams["grid.linewidth"] = 0.75
matplotlib.rcParams["lines.linewidth"] = 0.75
matplotlib.rcParams["patch.linewidth"] = 0.75
matplotlib.rcParams["lines.markersize"] = 8


matplotlib.rcParams["xtick.major.size"] = 3
matplotlib.rcParams["ytick.major.size"] = 3

matplotlib.rcParams["pdf.fonttype"] = 42
matplotlib.rcParams["ps.fonttype"] = 42
matplotlib.rcParams["font.size"] = 7
matplotlib.rcParams["axes.titlesize"] = matplotlib.rcParams["axes.labelsize"]
import matplotlib.pyplot as plt


name_map = {'MergeRank': 'BatchRank', 'BubbleRank': 'BubbleRank', 'CascadeKL-UCB': 'CascadeKL-UCB',
             'InitialPolicy': 'Baseline', 'BaselineRank': 'Baseline', 'TopRank': 'TopRank'}

color_map = {'MergeRank': 'blue', 'BubbleRank': 'red', 'CascadeKL-UCB': 'green',
             'InitialPolicy': 'grey', 'BaselineRank': 'grey', 'TopRank': 'orange'}

line_map = {'MergeRank': ':', 'BubbleRank': '--', 'CascadeKL-UCB': '-.',
             'InitialPolicy': '-', 'BaselineRank': '-', 'TopRank': '-'}

marker_map = {'MergeRank': '1', 'BubbleRank': '+', 'CascadeKL-UCB': '3',
             'InitialPolicy': '_', 'BaselineRank': '_', 'TopRank': '2'}


QUERY = []
    
results_type_keys = ['Regret', 'nDCG', 'nDCG_pow_rel', 'weak Violation', 'Violation']


def my_load_all(user, n_top, iteration=5000000, scale=10):
    # Result format: regret, nDCG, nDCG_power_rel, conservative, strong conservative
    if os.path.isfile('../results/%d/' % iteration + user + str(n_top) + '.pkl'):
        return pickle.load(open('../results/%d/' % iteration + user + str(n_top) + '.pkl'))
    results = {}
    for q in QUERY:
        dir_name = '../results/%d/'%iteration + user+str(n_top) + '/KLUCB/' + q + '/'
        dirs = os.listdir(dir_name)
        for d in dirs:
            if d == '.DS_Store':
                continue
            if d not in results.keys():
                results[d] = {'swap': [], 'n_runs': 0, 'Regret': [np.zeros(iteration/scale)]*2,
                'nDCG': [np.zeros(iteration/scale)]*2, 'nDCG_pow_rel': [np.zeros(iteration/scale)]*2,
                'weak Violation': [np.zeros(iteration/scale)]*2, 'Violation': [np.zeros(iteration/scale)]*2}

            methods_name = os.listdir(dir_name + d)
            for mn in methods_name:
                if mn == 'DS_Store':
                    continue
                d_name = float(mn.split('-')[0])
                try:
                    rt = pickle.load(open(dir_name + d + '/' + mn))
                except:
                    print 'fail'+ q
                results[d]['swap'].append([d_name, rt[0][-1]])
                results[d]['n_runs'] += 1
                for idx_key, key in enumerate(results_type_keys):
                    if 'Violation' in key:
                        results[d][key][0] = results[d][key][0] + np.cumsum(rt[idx_key])
                        results[d][key][1] = results[d][key][1] + np.cumsum(rt[idx_key]) ** 2
                    else:
                        results[d][key][0] = results[d][key][0] + np.asarray(rt[idx_key])
                        results[d][key][1] = results[d][key][1] + np.asarray(rt[idx_key]) ** 2

    for m_key in results.keys():
        print m_key + '=' * 10
        for key in results_type_keys:
            results[m_key][key][0] = results[m_key][key][0] / results[m_key]['n_runs']
            results[m_key][key][1] = results[m_key][key][1] / results[m_key]['n_runs']
            results[m_key][key][1] = np.sqrt(results[m_key][key][1] - results[m_key][key][0]**2) / np.sqrt(
                results[m_key]['n_runs'])
            print key + ': %f std: %f' % (results[m_key][key][0][-1], results[m_key][key][1][-1])
    dir_name = '../results/%d/' % iteration + user + str(n_top) + '.pkl'
    with open(dir_name, 'wb') as f:
        pickle.dump(results, f)
    return results


def my_plot(user, results, legend=False, scale=1000, result_type=0, extra_scale=100):
    # plot curves

    keys = results.keys()
    if result_type == 1:
        keys = ['BubbleRank', 'MergeRank', 'BaselineRank', 'TopRank']
    if result_type in [3, 4]: 
        keys = ['MergeRank', 'CascadeKL-UCB', 'BubbleRank', 'TopRank']

    step = results['BubbleRank']['Regret'][0].shape[0]
    x_range = []
    for i in range(1, int(np.ceil(np.log(extra_scale)/np.log(10)))):
        x_range += range(10**i, 10**(i+1), 10**i)
    else: 
        x = np.arange(step) *scale
    x_range += range(extra_scale, scale*step+1,extra_scale)
    x = np.asarray(x_range)
    [plt.plot(x, results[m_key][results_type_keys[result_type]][0][x/scale-1], 
        color=color_map[m_key], linestyle=line_map[m_key], marker=marker_map[m_key], markevery=0.1)
     for m_key in keys]
    
    [plt.fill_between(x, results[m_key][results_type_keys[result_type]][0][x/scale-1] - 
        results[m_key][results_type_keys[result_type]][1][x/scale-1], 
        results[m_key][results_type_keys[result_type]][0][x/scale-1] + 
        results[m_key][results_type_keys[result_type]][1][x/scale-1], 
        alpha=0.2, color=color_map[m_key]) for m_key in keys]
    plt.xlabel('Step n')
    axes = plt.gca()
    plt.xscale('log')
    plt.yscale('log')
    axes.set_xlim([scale, x[-1]])
    plt.tight_layout()
    axes.set_ylim(ymin=0.1)
    axes.tick_params(axis='y', which='minor', left='on')
    if legend:
        plt.legend([name_map[m_key] for m_key in keys], loc='upper left', framealpha=0.0)
    axes.tick_params(axis='y', which='minor', left='on')
    plt.xlim([10, int(5e6)])
    # plt.xticks([1e1, 1e2, 1e3, 1e4, 1e5, 1e6, 5e6])
    plt.xticks([1e1, 1e4, 1e6])
    if 'Regret' in results_type_keys[result_type]:
        plt.ylim([1, int(1e5)])
        plt.ylabel(results_type_keys[result_type])
    if 'nDCG' in results_type_keys[result_type]:
        plt.yscale('linear')
        plt.ylim([0.55, 1.02])
        plt.yticks([0.6, .7, 0.8, .9, 1.0])
        plt.ylabel('NDCG@5')
    if 'weak' in results_type_keys[result_type]:
        plt.ylim([1, 1e5])
        plt.ylabel('Violation')
    if 'Violation' == results_type_keys[result_type]:
        plt.ylim([1, 1e5])
        plt.ylabel('Violation')
    plt.title(user)

    axes.tick_params(axis='y', which='minor', left='on')
    plt.tight_layout(pad=0.2)
    return 0


def generate_table(user, rt):
    idx = np.asarray([1, 10, 100, int(1e3), int(1e4), int(1e5), int(5e5)])-1
    key_map = {'MergeRank': 'BatchRank', 'CascadeKL-UCB': 'CascadeKL-UCB', 'TopRank': 'TopRank', 'BubbleRank': 'BubbleRank'}
    print '\\toprule'
    print user + ' &' + ' &'.join(['10', '100', '1k', '10k', '100k', '1M', '5M']) + '\\\\'
    print'\\midrule'
    for key in key_map.keys():
        s = key_map[key]
        for mean, std in zip(rt[key]['weak Violation'][0][idx], rt[key]['weak Violation'][1][idx]):
            s += ' &$%.2f_{\\pm %.2f}$' %(mean, std)
        s += '\\\\'
        print s
    print '\\midrule'

    print'\\midrule'
    for key in key_map.keys():
        s = key_map[key]
        for mean, std in zip(rt[key]['Regret'][0][idx], rt[key]['Regret'][1][idx]):
            s += ' &$%.2f_{\\pm %.2f}$' % (mean, std)
        s += '\\\\'
        print s
    print '\\midrule'


def plot_swap(user, results):
    swap = results['BubbleRank']['swap']
    regret_dict = {}
    for sp in swap:
        regret_dict[str(sp[0])] = []
    for sp in swap:
        regret_dict[str(sp[0])].append(sp[1])
    keys = regret_dict.keys()
    print [[key, len(regret_dict[key]), np.mean(regret_dict[key]),
            np.std(regret_dict[key])/np.sqrt(len(regret_dict[key]))] for key in sorted(keys)]
    print np.sum([len(regret_dict[key]) for key in keys])
    x = [float(key) for key in keys if key != '35']
    y = [np.mean(regret_dict[key]) for key in keys if key != '35']
    st = [np.std(regret_dict[key])/np.sqrt(len(regret_dict[key])) for key in keys if key != '35']
    y_ = [max(y[i] - st[i], 0) for i in range(len(y))]
    y_min = [y[i] - y_[i] for i in range(len(y))]
    plt.errorbar(x, y, yerr=[y_min, st], fmt='.', color=color_map['BubbleRank'], ecolor=color_map['BubbleRank'],
                 markersize=6)
    plt.ylabel(r'Regret at $5\times 10^6$')
    plt.xlabel(r'$|\mathcal{V}_0|$')
    plt.title(user)
    axes = plt.gca()
    axes.set_xlim([np.min(x)-1, 35])
    if user == 'CM':
        axes.set_ylim([0, 8000])
        plt.yticks(1000 * np.asarray([0, 2, 4, 6, 8]), ["0", "2k", "4k", "6k", "8k"])
    elif user == 'DCM':
        axes.set_ylim([0, 9000])
        plt.yticks(1000 * np.asarray([0, 3, 6, 9]), ["0", "3k", "6k", "9k"])
    else:
        axes.set_ylim([0, 33000])
        plt.yticks(1000 * np.asarray([0, 8, 16, 24, 32]), ["0", "8k", "16k", "24k", "32k"])
    plt.tight_layout(pad=0.2)
    return 0


if __name__ == '__main__':
    data_names = ['DCM', 'CM', 'PBM']
    # data_names = ['CM']
    for data_name in data_names:
        # print data_name + '='*10
        iters =5000000

        # Main results
        rt = my_load_all(data_name, n_top=5, iteration=iters, scale=10)
        result_type = [(0, 'Regret'), (1, 'nDCG'), (3, 'Violation'), (4, 'Strong')]
        for rt_idx, rt_item in result_type:
            fig = plt.figure(figsize=(1.55, 1.4))
            if rt_item == 'Regret' and data_name == 'CM':
                my_plot(user=data_name, results=rt, legend=False, scale=10, result_type=rt_idx, extra_scale=1000000)
            else:
                my_plot(user=data_name, results=rt, legend=False, scale=10, result_type=rt_idx, extra_scale=1000000)
            # fig.savefig('../figures/wwwfigures/'+data_name + '-' + rt_item+'.pdf', dpi=1200, bbox_inches=0)
            fig.savefig('../figures/wwwfigures/'+data_name + '-' + rt_item+'.png', dpi=400, bbox_inches=0)

        # # table
        # generate_table(data_name, rt)

        # swap
        d_name = '../results/different-initial-%d/' % iters + data_name + '5' + '.pkl'
        rt = pickle.load(open(d_name))
        # fig = plt.figure(figsize=(1.6, 1.6))  # uai 19
        fig = plt.figure(figsize=(2.35, 1.3))  # thesis
        plot_swap(data_name, rt)
        # fig.savefig('../figures/wwwfigures/' + data_name + '-swap.pdf', dpi=120, bbox_inches=0)
        fig.savefig('../figures/wwwfigures/' + data_name + '-swap.png', dpi=400, bbox_inches=0)