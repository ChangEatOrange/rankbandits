import numpy as np
import os
import matplotlib
import pickle 

matplotlib.rcParams["axes.linewidth"] = 0.75
matplotlib.rcParams["grid.linewidth"] = 0.75
matplotlib.rcParams["lines.linewidth"] = 0.75
matplotlib.rcParams["patch.linewidth"] = 0.75
matplotlib.rcParams["lines.markersize"] = 8


matplotlib.rcParams["xtick.major.size"] = 3
matplotlib.rcParams["ytick.major.size"] = 3

matplotlib.rcParams["pdf.fonttype"] = 42
matplotlib.rcParams["ps.fonttype"] = 42
matplotlib.rcParams["font.size"] = 7
matplotlib.rcParams["axes.titlesize"] = matplotlib.rcParams["axes.labelsize"]
import matplotlib.pyplot as plt


name_map = {'MergeRank': 'BatchRank',
            'CascadeKL-UCB': 'CascadeKL-UCB',
            'CascadeEXP3': 'CascadeEXP3',
            'CascadeWUCB': 'CascadeSWUCB',
            'CascadeWUCB0': 'CascadeSWUCB0',
            'NewCascadeWUCB': 'NewCascadeSWUCB',
            'NewCascadeWUCB0': 'NewCascadeSWUCB0',
            'CascadeDUCB': 'CascadeDUCB',
            'CascadeDUCB0': 'CascadeDUCB0',
            'CascadeRexp3': 'CascadeRexp3',
            'CascadeWKL-UCB': 'CascadeWKL-UCB',
            'CascadeDKL-UCB': 'CascadeDKL-UCB',
            'RankedEXP3': 'RankedEXP3'
            }
color_map = {'MergeRank': 'blue', 'CascadeKL-UCB': 'green',
             'CascadeWUCB': 'red', 'CascadeDUCB': 'black',
             'CascadeRexp3': 'gray', 'CascadeWKL-UCB': 'yellow',
             'CascadeDKL-UCB': 'cyan', 'CascadeEXP3': 'pink',
             'RankedEXP3': 'cyan', 'CascadeDUCB0': 'orange',
             'CascadeWUCB0': 'purple',
             'NewCascadeWUCB0': 'pink', 'NewCascadeWUCB': 'gray'}

line_map = {'MergeRank': ':', 'CascadeKL-UCB': '--', 'CascadeWUCB': '-.',
             'CascadeDUCB': '-', 'CascadeRexp3': '-', 'CascadeWKL-UCB': '-',
            'CascadeDKL-UCB': '-', 'CascadeEXP3': '-', 'RankedEXP3': ':',
            'CascadeWUCB0': '-', 'CascadeDUCB0': '-',
            'NewCascadeWUCB0': '-', 'NewCascadeWUCB': '-'}

marker_map = {'MergeRank': '1', 'CascadeWUCB': '+', 'CascadeKL-UCB': '3',
             'CascadeDUCB': '2', 'CascadeRexp3': '_', 'CascadeWKL-UCB': '4',
             'CascadeDKL-UCB': 'x', 'CascadeEXP3': 'o', 'RankedEXP3': '|',
             'CascadeWUCB0': 5, 'CascadeDUCB0': 6,
             'NewCascadeWUCB0': 7, 'NewCascadeWUCB': 8}


QUERY = []
    
results_type_keys = ['Regret', 'nDCG', 'nDCG_pow_rel', 'weak Violation', 'Violation']


def my_load_all(user, n_top, iteration=100000, n_abrupt=100, scale=10):
    # Result format: regret, nDCG, nDCG_power_rel, conservative, strong conservative
    if os.path.isfile('../results/nonstationary/%d/%d/' % (iteration, n_abrupt) + user + str(n_top) + '.pkl'):
        return pickle.load(open('../results/nonstationary/%d/%d/' % (iteration, n_abrupt) + user + str(n_top) + '.pkl'))
    else:
        raise Exception('call nonstationary-load-raw-results.py')


def my_plot(user, results, legend=False, scale=1000, result_type=0, extra_scale=100):
    # plot curves

    keys = results.keys()

    keys = ['RankedEXP3', 'MergeRank', 'CascadeKL-UCB', 'CascadeDUCB', 'CascadeWUCB']
    # keys = ['CascadeDUCB', 'CascadeDUCB0', 'NewCascadeWUCB', 'CascadeWUCB', 'NewCascadeWUCB0', 'CascadeWUCB0']
    step = results['CascadeKL-UCB']['Regret'][0].shape[0]
    x_range = []
    for i in range(1, int(np.ceil(np.log(extra_scale)/np.log(10)))):
        x_range += range(10**i, 10**(i+1), 10**i)
    else: 
        x = np.arange(step) *scale
    x_range += range(extra_scale, scale*step+1,extra_scale)
    x = np.asarray(x_range)
    [plt.plot(x, results[m_key][results_type_keys[result_type]][0][x/scale-1], 
        color=color_map[m_key], linestyle=line_map[m_key], marker=marker_map[m_key], markevery=0.1)
     for m_key in keys]
    
    [plt.fill_between(x, results[m_key][results_type_keys[result_type]][0][x/scale-1] - 
        results[m_key][results_type_keys[result_type]][1][x/scale-1], 
        results[m_key][results_type_keys[result_type]][0][x/scale-1] + 
        results[m_key][results_type_keys[result_type]][1][x/scale-1], 
        alpha=0.2, color=color_map[m_key]) for m_key in keys]
    plt.xlabel('Step n')
    axes = plt.gca()
    # plt.xscale('log')
    # plt.yscale('log')
    axes.set_xlim([scale, x[-1]])
    plt.tight_layout()
    axes.set_ylim(ymin=0.1)
    axes.tick_params(axis='y', which='minor', left='on')

    axes.tick_params(axis='y', which='minor', left='on')
    plt.xlim([0, int(1e5)])
    plt.xticks([0, 2e4, 4e4, 6e4, 8e4, 1e5], ['0', '20k', '40k', '60k', '80k', '100k'])
    if 'Regret' in results_type_keys[result_type]:
        if user == 'CM':
            plt.ylim([1, int(5e3)])
            plt.ylabel(results_type_keys[result_type])
            plt.yticks([0, 1e3, 2e3, 3e3, 4e3, 5e3], ['0', '1k', '2k', '3k', '4k', '5k'])
    if 'nDCG' in results_type_keys[result_type]:
        plt.yscale('linear')
        plt.ylim([0.55, 1.02])
        plt.yticks([0.6, .7, 0.8, .9, 1.0])
        plt.ylabel('NDCG@5')
        legend = False
    if 'weak' in results_type_keys[result_type]:
        plt.ylim([1, 1e5])
        plt.ylabel('Violation')
    if 'Violation' == results_type_keys[result_type]:
        plt.ylim([1, 1e5])
        plt.ylabel('Violation')
    # plt.title(user)

    if legend:
        plt.legend([name_map[m_key] for m_key in keys], loc='upper left', bbox_to_anchor=(0, 1.0), framealpha=0.0)
    axes.tick_params(axis='y', which='minor', left='on')
    plt.tight_layout(pad=0.2)
    return 0


if __name__ == '__main__':
    # data_names = ['CM', 'PBM']
    data_names = ['CM']
    for data_name in data_names:
        iters =100000
        for n_abrupts in [10]:#[5, 10, 20]:
            for n_top in [3]:
                rt = my_load_all(data_name, n_top=n_top, iteration=iters, n_abrupt=n_abrupts, scale=10)
                result_type = [(0, 'Regret'), (1, 'nDCG'), (3, 'Violation'), (4, 'Strong')]
                result_type = [(0, 'Regret')]
                for rt_idx, rt_item in result_type:
                    # if rt_item == 'Regret':
                    # fig = plt.figure(figsize=(3.35, 2.2)) # IJCAI 19
                    fig = plt.figure(figsize=(4.7, 3))  # THESIS
                    my_plot(user=data_name, results=rt, legend=True, scale=10, result_type=rt_idx, extra_scale=iters)
                    # fig.savefig('../figures/nonstationary/' + '%d-%d-%d-' % (n_top, iters, n_abrupts) + data_name + '-' + rt_item +'.pdf', dpi=1200, bbox_inches=0)
                    fig.savefig('../figures/nonstationary/' + '%d-%d-%d-' % (n_top, iters, n_abrupts) + data_name + '-' + rt_item +'.png', dpi=400, bbox_inches=0)
