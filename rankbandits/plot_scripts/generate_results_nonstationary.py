import numpy as np
import os
import matplotlib
import pickle as pk

# font = {'family': 'Times New Roman',
#         #'weight': 'bold',
#         'size': 7}

# matplotlib.rc('font', **font)

matplotlib.rcParams["axes.linewidth"] = 0.75
matplotlib.rcParams["grid.linewidth"] = 0.75
matplotlib.rcParams["lines.linewidth"] = 0.75
matplotlib.rcParams["patch.linewidth"] = 0.75
matplotlib.rcParams["xtick.major.size"] = 3
matplotlib.rcParams["ytick.major.size"] = 3

matplotlib.rcParams["pdf.fonttype"] = 42
matplotlib.rcParams["ps.fonttype"] = 42
matplotlib.rcParams["font.size"] = 7
matplotlib.rcParams["axes.titlesize"] = matplotlib.rcParams["axes.labelsize"]
import matplotlib.pyplot as plt


USER = ['CM', 'PBM']
USER = ['CM']
N_TOP = [3]
QUERY = ['q214456', 'q3866', 'q132308', 'q213099', 'q269906']


name_map = {'MergeRank': 'BatchRank',
            'CascadeKL-UCB': 'CascadeKL-UCB',
            'CascadeEXP3': 'CascadeEXP3',

            'CascadeWUCB': 'CascadeSWUCB',
            'CascadeWUCB0': 'CascadeSWUCB0',
            'NewCascadeWUCB': 'CascadeWUCB',
            'NewCascadeWUCB0': 'NewCascadeSWUCB0',

            'CascadeDUCB': 'CascadeDUCB',
            'CascadeDUCB0': 'CascadeDUCB0',
            'CascadeRexp3': 'CascadeRexp3',
            'CascadeWKL-UCB': 'CascadeWKL-UCB',
            'CascadeDKL-UCB': 'CascadeDKL-UCB',
            'RankedEXP3': 'RankedEXP3'
            }

color_map = {'MergeRank': 'blue', 'CascadeKL-UCB': 'green',
             'CascadeWUCB': 'red', 'CascadeDUCB': 'black',
             'CascadeRexp3': 'gray', 'CascadeWKL-UCB': 'yellow',
             'CascadeDKL-UCB': 'cyan', 'CascadeEXP3': 'pink',
             'RankedEXP3': 'cyan', 'CascadeDUCB0': 'orange',
             'CascadeWUCB0': 'purple',
             'NewCascadeWUCB0': 'pink', 'NewCascadeWUCB': 'red'}

line_map = {'MergeRank': ':', 'CascadeKL-UCB': '--', 'CascadeWUCB': '-.',
             'CascadeDUCB': '-', 'CascadeRexp3': '-', 'CascadeWKL-UCB': '-',
            'CascadeDKL-UCB': '-', 'CascadeEXP3': '-', 'RankedEXP3': ':',
            'CascadeWUCB0': '-', 'CascadeDUCB0': '-',
            'NewCascadeWUCB0': '-', 'NewCascadeWUCB': '-.'}

marker_map = {'MergeRank': '1', 'CascadeWUCB': '+', 'CascadeKL-UCB': '3',
             'CascadeDUCB': '2', 'CascadeRexp3': '_', 'CascadeWKL-UCB': '4',
             'CascadeDKL-UCB': 'x', 'CascadeEXP3': 'o', 'RankedEXP3': '|',
             'CascadeWUCB0': 5, 'CascadeDUCB0': 6,
             'NewCascadeWUCB0': 7, 'NewCascadeWUCB': '+'}


def my_plot(user, results, legend=False, plot_scale=50):
    # plot curves
    # fig = plt.figure(figsize=(2.75, 1.5))

    keys = results.keys()
    # keys = ['BubbleRank', 'BatchRank', 'CombineRanker', 'CascadingKL-UCB']

    d_plot = []
    std_plot = []
    std_min = []
    std_max = []

    for key in results.keys():
        mean_tmp = np.zeros(results[key][0][1][0].shape[0]+1)
        mean_tmp[1:] = np.mean([item[1][0] for item in results[key]], axis=0)
        d_plot.append(mean_tmp)

        std_tmp = np.zeros(results[key][0][1][0].shape[0]+1)
        std_tmp[1:] = np.std([item[1][0] for item in results[key]], axis=0)/np.sqrt(len(results[key]))
        std_plot.append(std_tmp)
        std_min_tmp = np.zeros(results[key][0][1][0].shape[0]+1)
        std_min_tmp[1:] = np.min([item[1][0] for item in results[key]], axis=0)
        std_min.append(std_min_tmp)

        std_max_tmp = np.zeros(results[key][0][1][0].shape[0]+1)
        std_max_tmp[1:] = np.max([item[1][0] for item in results[key]], axis=0)
        std_max.append(std_max_tmp)
        print key, 'r@T: ', mean_tmp[-1], 'std: ', std_tmp[-1]
    x = np.arange(d_plot[0].shape[0]) * 10

    x = x[::plot_scale]
    [plt.plot(x, d_plot[p_idx][::plot_scale], color=color_map[keys[p_idx]],
              linestyle=line_map[keys[p_idx]], marker=marker_map[keys[p_idx]], markevery=0.1
              ) for p_idx in range(len(d_plot))]
    [plt.fill_between(x, d_plot[p_idx][::plot_scale] - std_plot[p_idx][::plot_scale],
                      d_plot[p_idx][::plot_scale] + std_plot[p_idx][::plot_scale],
                      alpha=0.2, color=color_map[keys[p_idx]]) for p_idx in range(len(d_plot))]
    # [plt.fill_between(x, std_min[p_idx], std_max[p_idx], alpha=0.2) for p_idx in range(len(d_plot))]
    # [plt.errorbar(x[::100], d_plot[p_idx][::100], xerr=0.0, yerr=std_plot[p_idx][::100]) for p_idx in range(len(d_plot))]

    # plt.xlabel('Step n', fontsize=7)
    axes = plt.gca()
    # plt.xscale('log')
    # plt.yscale('log')
    plt.xlim([0, int(1e5)])
    plt.xticks([0, 5e4, 1e5], ['0', '50k', '100k'])
    axes.tick_params(axis='y', which='minor', left='on')
    if user == 'CM':
        axes.set_ylim(ymin=1, ymax=10000)
        plt.yticks([0, 6000, 12000], ['0', '6k', '12k'])
        # plt.ylabel('Regret', fontsize=7)
        if legend:
            plt.legend([name_map[key] for key in keys], loc='upper left', bbox_to_anchor=(0.0, 1.0), framealpha=0.0)
    else:
        axes.set_ylim(ymin=1)
    return 0


def my_load(user, n_top, q):
    dir_name = '../results/nonstationary/100000/' + user+str(n_top) + '/10/' + q + '/'
    dirs = os.listdir(dir_name)
    results = {}
    for d in dirs:
        if d == '.DS_Store' or d in ['CascadeWUCB', 'CascadeWUCB0', 'CascadeDUCB0',
                                     'NewCascadeWUCB0', 'CascadeDKL-UCB',
                                     'CascadeWKL-UCB']:
            continue
        if d not in results.keys():
            results[d] = []

        methods_name = os.listdir(dir_name + d)
        if len(methods_name) < 1:
            print(dir_name+d + '  does not have results')
            return False
        for mn in methods_name:
            if mn == '.DS_Store':
                continue
            d_name = float(mn.split('-')[0])
            results[d].append([d_name, pk.load(open(dir_name + d + '/' + mn))])
    return results


def draw_ave(q):
    # draw cumulative curves for q
    print '---------------\n' + 'Query id:' + q + '\n---------------'
    for n_top in N_TOP:
        print '@ top' + str(n_top) 
        # fig = plt.figure(figsize=(1.7, 1.65))  # ijcai
        fig = plt.figure(figsize=(1.15, 1.12))   # thesis
        nn=1
        for user in USER:
            print 'User:' + user + '\n---------------'            
            # load data
            results = my_load(user, n_top, q)
            if not results:
                continue
            # plt.subplot(1, 2, nn)
            nn+=1
            fig1 = my_plot(user, results)
            plt.title('query ' + q[1:])
        plt.tight_layout(pad=0.2)
        # plt.savefig('../figures/nonstationary/single/'+q+'-'+str(n_top) + '.pdf', dpi=1200, bbox_inches=0)
        plt.savefig('../figures/nonstationary/single/'+q+'-'+str(n_top) + '.png', dpi=400, bbox_inches=0)


if __name__ == '__main__':
    QUERY = os.listdir('../results/nonstationary/100000/CM3/10')
    if '.DS_Store' in QUERY:
        QUERY.pop(QUERY.index('.DS_Store'))
    print len(QUERY)
    QUERY = ['q325', 'q2241', 'q3866', 'q11527', 'q15217', 'q15925', 'q19815', 'q22500', 'q28658', 'q12503955',
             'q17112114', 'q10434642', 'q486195', 'q373015', 'q368794', 'q310450', 'q297115', 'q269906',
             'q266608', 'q258892']
    for q in QUERY:
        if q == '.DS_Store':
            continue
        draw_ave(q)

