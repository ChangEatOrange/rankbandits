import numpy as np
import os
import pickle as pk
import matplotlib

# font = {'family': 'Times New Roman',
#         #'weight': 'bold',
#         'size': 7}

# matplotlib.rc('font', **font)

matplotlib.rcParams["axes.linewidth"] = 0.75
matplotlib.rcParams["grid.linewidth"] = 0.75
matplotlib.rcParams["lines.linewidth"] = 0.75
matplotlib.rcParams["patch.linewidth"] = 0.75
matplotlib.rcParams["xtick.major.size"] = 3
matplotlib.rcParams["ytick.major.size"] = 3

matplotlib.rcParams["pdf.fonttype"] = 42
matplotlib.rcParams["ps.fonttype"] = 42
matplotlib.rcParams["font.size"] = 7
matplotlib.rcParams["axes.titlesize"] = matplotlib.rcParams["axes.labelsize"]
import matplotlib.pyplot as plt


USER = ['CM', 'PBM']
N_TOP = [3, 5]
QUERY = ['q214456', 'q213099']
METHOD = ['CascadeKL-UCB']

def draw(q):
    # load data
    print '---------------\n' + 'Query id:' + q + '\n---------------'
    for user in USER:
        print 'User:' + user + '\n---------------'
        for n_top in N_TOP:
            print '@ top' + str(n_top)
            # load data
            dir_name = '../results/quality/' + user + '/KLUCB/' + q + '/'
            dirs = os.listdir(dir_name)
            results = {}
            for d in dirs:
                if d == '.DS_Store':
                    continue
                f = open(dir_name + d)
                results[d] = pk.load(f)
        # plot curves
        plt.figure(figsize=(8, 12), dpi=80)
        x = [20*ii for ii in range(1, 11)]
        for bkey in results.keys():
            for idx, item in enumerate(['n_ctr3', 'n_ctr5']):
                y = np.mean(results[bkey][item], axis=0)
                plt.subplot(2, 1, idx + 1)
                plt.plot(x, y[:10])
                plt.plot(x, [y[-1]]*10)
                plt.legend(['Results after x time-steps', 'Results after $10^3$ time-steps'])
                plt.ylabel(item[:-1] +'@'+ item[-1])
                plt.xlabel('Time-step')
                plt.title(bkey[:-3])
                axes = plt.gca()
                axes.set_xlim([20, 200])
        plt.savefig('../figures/quality/'+ q + '-' + user + '-' + bkey[:-3] +'.pdf' )

def draw_all():
    # load data
    for user in USER:
        print 'User:' + user + '\n---------------'
        for n_top in N_TOP:
            print '@ top' + str(n_top)
            # load data
            querys = os.listdir('../results/quality/' + user + '/KLUCB/')
            results = {}
            for q in querys: 
                if q == '.DS_Store':
                    continue
                dir_name = '../results/quality/' + user + '/KLUCB/' + q + '/'
                dirs = os.listdir(dir_name)
                for d in dirs:
                    if d == '.DS_Store':
                        continue
                    f = open(dir_name + d)
                    if d not in results.keys():
                        results[d] = {'n_ctr3': [], 'n_ctr5': []}
                    q_results = pk.load(f)
                    for key in results[d].keys():
                        results[d][key].append(np.mean(q_results[key], axis=0))

        # plot curves
        plt.figure(figsize=(8, 12), dpi=80)
        x = [20*ii for ii in range(1, 11)]
        for bkey in results.keys():
            for idx, item in enumerate(['n_ctr3', 'n_ctr5']):
                y = np.mean(results[bkey][item], axis=0)
                plt.subplot(2, 1, idx + 1)
                plt.plot(x, y[:10])
                plt.plot(x, [y[-1]]*10)
                plt.legend(['Results after x time-steps', 'Results after $10^3$ time-steps'])
                plt.ylabel(item[:-1] +'@'+ item[-1])
                plt.xlabel('Time-step')
                plt.title(bkey[:-3])
                axes = plt.gca()
                axes.set_xlim([20, 200])
        plt.savefig('../figures/quality/ave-' + user + '-' + bkey[:-3] +'.pdf' )



def draw_estimator():
    # load data
    user = 'PBM5'
    n_top = 5
    # load data
    base_dir = '../results/estimator/1000000/' + user + '/KLUCB/'
    querys = os.listdir(base_dir)
    results = {}
    for q in querys: 
        if q == '.DS_Store':
            continue
        dir_name = base_dir + q + '/BubbleRank/'
        dirs = os.listdir(dir_name)
        if q not in results.keys():
            results[q] = []
        for d in dirs:
            if d == '.DS_Store':
                continue
            q_results = np.load(dir_name + d)            
            results[q].append(q_results)

    # plot curves
    # plt.figure(figsize=(1.6, 1.6))  # UAI 19
    plt.figure(figsize=(2.35, 1.3))  # Thesis
    keys = results.keys()
    keys.sort(key=float)
    keys = keys[::-1]
    for key in keys:
        x = np.arange(len(results[key][0]))*1000
        p_mean = np.mean(results[key], axis=0)
        print key, p_mean[-1]
        p_std = np.std(results[key], axis=0) / np.sqrt(len(results[key]))
        print len(results[key])
        plt.plot(x, p_mean)
        plt.fill_between(x, p_mean - p_std, p_mean + p_std, alpha=0.2)
    plt.ylabel('Regret')
    plt.xlabel('Step n')
    k_name = [r'$\chi_{min} = 0.5^%d$'% int(key) for key in keys]
    axes = plt.gca()
    axes.set_xlim([0, 1000000])
    axes.set_ylim(ymin=0)
    plt.xticks(100000 * np.arange(0, 11, 5), ["0", "0.5M", "1M"])
    plt.yticks(1000 * np.arange(0, 16, 5), ["0", "5k", "10k", "15k"])
    plt.title(r'Dependence on $1/\chi_{min}$')
    # plt.title('')
    # plt.legend(k_name, framealpha=0.0, labelspacing=0.)
    plt.tight_layout(pad=0.2)
    plt.savefig('../figures/estimator/' + 'bubblerank.pdf', dpi=120, bbox_inches=0 )
    plt.savefig('../figures/estimator/' + 'bubblerank.png', dpi=400, bbox_inches=0 )



if __name__ == '__main__':
    # for q in QUERY:
    #     draw(q)
    draw_estimator()