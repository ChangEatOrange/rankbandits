import numpy as np
import os
import pickle


"""
Process on the data from slurm and transfer them to .pkl files.
For the main resutls.  
"""

name_map = {'MergeRank': 'BatchRank', 'BubbleRank': 'BubbleRank', 'CascadeKL-UCB': 'CascadeKL-UCB',
            'InitialPolicy': 'Baseline', 'BaselineRank': 'Baseline'}

color_map = {'MergeRank': 'blue', 'BubbleRank': 'green', 'CascadeKL-UCB': 'cyan',
             'InitialPolicy': 'grey', 'BaselineRank': 'grey'}

QUERY = []

results_type_keys = ['Regret', 'nDCG', 'nDCG_pow_rel', 'weak Violation', 'Violation']


def my_load_all(user, n_top, iteration=5000000, scale=10):
    # Result format: regret, nDCG, nDCG_power_rel, conservative, strong conservative
    results = {}
    for q in QUERY:
        dir_name = '../results/%d/' % iteration + user + str(n_top) + '/KLUCB/' + q + '/'
        dirs = os.listdir(dir_name)
        for d in dirs:
            if d == '.DS_Store':
                continue
            if d not in results.keys():
                results[d] = {'swap': [], 'n_runs': 0, 'Regret': [np.zeros(iteration / scale)] * 2,
                              'nDCG': [np.zeros(iteration / scale)] * 2,
                              'nDCG_pow_rel': [np.zeros(iteration / scale)] * 2,
                              'weak Violation': [np.zeros(iteration / scale)] * 2,
                              'Violation': [np.zeros(iteration / scale)] * 2}

            methods_name = os.listdir(dir_name + d)
            for mn in methods_name:
                if mn == 'DS_Store':
                    continue
                d_name = float(mn.split('-')[0])
                try:
                    rt = pickle.load(open(dir_name + d + '/' + mn))
                except:
                    print 'fail' + q
                results[d]['swap'].append([d_name, rt[0][-1]])
                results[d]['n_runs'] += 1
                for idx_key, key in enumerate(results_type_keys):
                    if 'Violation' in key:
                        results[d][key][0] = results[d][key][0] + np.cumsum(rt[idx_key])
                        results[d][key][1] = results[d][key][1] + np.cumsum(rt[idx_key]) ** 2
                    else:
                        results[d][key][0] = results[d][key][0] + np.asarray(rt[idx_key])
                        results[d][key][1] = results[d][key][1] + np.asarray(rt[idx_key]) ** 2

    for m_key in results.keys():
        print m_key + '=' * 10
        for key in results_type_keys:
            results[m_key][key][0] = results[m_key][key][0] / results[m_key]['n_runs']
            results[m_key][key][1] = results[m_key][key][1] / results[m_key]['n_runs']
            results[m_key][key][1] = np.sqrt(results[m_key][key][1] - results[m_key][key][0] ** 2) / np.sqrt(
                results[m_key]['n_runs'])
            print key + ': %f std: %f' % (results[m_key][key][0][-1], results[m_key][key][1][-1])
    dir_name = '../results/%d/' % iteration + user + str(n_top) + '.pkl'
    with open(dir_name, 'wb') as f:
        pickle.dump(results, f)
    return results


if __name__ == '__main__':
    data_names = ['CM', 'PBM']
    # data_names = ['DCM']
    for data_name in data_names:
        print data_name + '=' * 10
        iters = 5000000
        QUERY = os.listdir('../results/%d/' % iters + data_name + '5/KLUCB')
        if '.DS_Store' in QUERY:
            QUERY.pop(QUERY.index('.DS_Store'))
        rt = my_load_all(data_name, n_top=5, iteration=iters, scale=10)
