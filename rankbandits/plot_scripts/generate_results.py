import numpy as np
import os
import matplotlib

# font = {'family': 'Times New Roman',
#         #'weight': 'bold',
#         'size': 7}

# matplotlib.rc('font', **font)

matplotlib.rcParams["axes.linewidth"] = 0.75
matplotlib.rcParams["grid.linewidth"] = 0.75
matplotlib.rcParams["lines.linewidth"] = 0.75
matplotlib.rcParams["patch.linewidth"] = 0.75
matplotlib.rcParams["xtick.major.size"] = 3
matplotlib.rcParams["ytick.major.size"] = 3

matplotlib.rcParams["pdf.fonttype"] = 42
matplotlib.rcParams["ps.fonttype"] = 42
matplotlib.rcParams["font.size"] = 7
matplotlib.rcParams["axes.titlesize"] = matplotlib.rcParams["axes.labelsize"]
import matplotlib.pyplot as plt


USER = ['CM', 'PBM']
# USER = ['CM']
N_TOP = [5]
QUERY = ['q214456', 'q3866', 'q132308', 'q213099', 'q269906']


name_map = {'CombineRankerFreq': 'BubbleRank-KL', 'MergeRank': 'BatchRank', 'BubbleRank': 'BubbleRank0', 'CombineRanker': 'BubbleRank-KL', 'CascadeKL-UCB': 'CascadeKL-UCB'}
color_map = {'CombineRankerFreq': 'red', 'MergeRank': 'blue', 'BubbleRank': 'green', 'CombineRanker': 'cyan', 'CascadeKL-UCB': 'cyan'}


def my_plot(user, results, legend=False):
    # plot curves
    # fig = plt.figure(figsize=(2.75, 1.5))

    # keys = ['BubbleRank', 'BatchRank', 'CombineRanker', 'CascadingKL-UCB']
    keys = results.keys()
    d_plot = []
    std_plot = []
    std_min = []
    std_max = []

    for key in results.keys():
        mean_tmp = np.zeros(results[key][0][1].shape[0]+1)
        mean_tmp[1:] = np.mean([item[1] for item in results[key]], axis=0)
        d_plot.append(mean_tmp)

        std_tmp = np.zeros(results[key][0][1].shape[0]+1)
        std_tmp[1:] = np.std([item[1] for item in results[key]], axis=0)/np.sqrt(len(results[key]))
        std_plot.append(std_tmp)
        std_min_tmp = np.zeros(results[key][0][1].shape[0]+1)
        std_min_tmp[1:] = np.min([item[1] for item in results[key]], axis=0) 
        std_min.append(std_min_tmp)

        std_max_tmp = np.zeros(results[key][0][1].shape[0]+1)
        std_max_tmp[1:] = np.max([item[1] for item in results[key]], axis=0) 
        std_max.append(std_max_tmp)
        print key, 'r@T: ', mean_tmp[-1], 'std: ', std_tmp[-1]
    x = np.arange(d_plot[0].shape[0]) * 1000
    [plt.plot(x, d_plot[p_idx], color=color_map[keys[p_idx]]) for p_idx in range(len(d_plot))]
    [plt.fill_between(x, d_plot[p_idx] - std_plot[p_idx], d_plot[p_idx] + std_plot[p_idx], alpha=0.2, color=color_map[keys[p_idx]]) for p_idx in range(len(d_plot))]
    # [plt.fill_between(x, std_min[p_idx], std_max[p_idx], alpha=0.2) for p_idx in range(len(d_plot))]
    # [plt.errorbar(x[::100], d_plot[p_idx][::100], xerr=0.0, yerr=std_plot[p_idx][::100]) for p_idx in range(len(d_plot))]
    plt.xlabel('Step n', fontsize=7)
    axes = plt.gca()
    plt.xscale('log')
    plt.yscale('log')
    axes.set_xlim([1e3, 1e7])
    axes.set_ylim(ymin=1)
    axes.tick_params(axis='y', which='minor', left='on')
    if user == 'CM':
        plt.yticks([1, 10, 100, 1000, 10000])
        plt.ylabel('Regret', fontsize=7)
        if legend:
            plt.legend([name_map[key] for key in keys], loc='upper left', bbox_to_anchor=(0.0, 1.0), framealpha=0.0)
    else:
        axes.set_ylim(ymin=10)
        plt.yticks([10, 100, 1000, 10000, 100000])
    return 0


def my_load_all(user, n_top):
    results = {}            
    for q in QUERY:
        dir_name = 'results/10020000/' + user+str(n_top) + '/KLUCB/' + q + '/'
        dirs = os.listdir(dir_name)
        for d in dirs:
            if d == '.DS_Store' or d == 'BatchRank' or d == 'CombineRanker' or d == 'InitialPolicy':
                continue
            if d not in results.keys():
                results[d] = []

            methods_name = os.listdir(dir_name + d)
            for mn in methods_name:
                if mn == 'DS_Store':
                    continue
                d_name = float(mn.split('-')[0])

                if d == 'CombineRankerFreq' or d == 'CombineRanker':
                    rt_tmp = np.load(dir_name + d + '/' + mn)
                    results[d].append([d_name, rt_tmp[20:] - rt_tmp[19]])
                else:
                    results[d].append([d_name, np.load(dir_name + d + '/' + mn)[:10000]])
    return results


def my_load(user, n_top, q):
    dir_name = 'results/mean-10000000/' + user+str(n_top) + '/KLUCB/' + q + '/'
    dirs = os.listdir(dir_name)
    results = {}
    for d in dirs:
        if d == '.DS_Store' or d == 'BatchRank' or d == 'CombineRanker':
            continue
        if d not in results.keys():
            results[d] = []

        methods_name = os.listdir(dir_name + d)
        if len(methods_name) < 3:
            return False
        for mn in methods_name:
            if mn == 'DS_Store':
                continue
            d_name = float(mn.split('-')[0])
            if d == 'CombineRankerFreq' or d == 'CombineRanker':
                rt_tmp = np.load(dir_name + d + '/' + mn)
                results[d].append([d_name, rt_tmp[20:9920] - rt_tmp[19]])
            else:
                results[d].append([d_name, np.load(dir_name + d + '/' + mn)[:9900]])
    return results


def draw_all_curves():
    # draw cumulative curves over all
    for n_top in N_TOP:
        fig = plt.figure(figsize=(5.5, 1.5))
        nn=1
        print '@ top' + str(n_top) 
        for user in USER:
            print 'User:' + user + '\n---------------'
            # load data        
            results = my_load_all(user, n_top)
            # plot
            plt.subplot(1,2,nn)
            nn+=1
            fig1 = my_plot(user, results, False)
            # plt.title('In ' + user + ' @'+str(n_top))
            plt.title(user)
        plt.tight_layout(pad=0.2)
        plt.savefig('figures/'+'regrets-ave@'+str(n_top) + '-curves.pdf', dpi=1200, bbox_inches=0)
        plt.savefig('figures/'+'regrets-ave@'+str(n_top) + '-curves', dpi=1200, bbox_inches=0)


def draw_ave(q):
    # draw cumulative curves for q
    print '---------------\n' + 'Query id:' + q + '\n---------------'
    for n_top in N_TOP:
        print '@ top' + str(n_top) 
        fig = plt.figure(figsize=(5.5, 1.5))
        nn=1
        for user in USER:
            print 'User:' + user + '\n---------------'            
            # load data
            results = my_load(user, n_top, q)
            if not results:
                continue
            plt.subplot(1, 2, nn)
            nn+=1
            fig1 = my_plot(user, results)
            plt.title(user + ' on query ' + q[1:])
        plt.tight_layout(pad=0.2)
        plt.savefig('figures/average-'+q+'@'+str(n_top) + '-curves.pdf', dpi=1200, bbox_inches=0)


def draw_all():
    for n_top in N_TOP:
        print '@ top' + str(n_top)
        plt.figure(figsize=(3.6, 1.5))
        nn = 1
        for user in USER:
            print 'User:' + user + '\n---------------'
            # load data
            results = {}
            for q in QUERY:
                dir_name = 'results/10020000/' + user+str(n_top) + '/KLUCB/' + q + '/'
                dirs = os.listdir(dir_name)
                for d in dirs:
                    if d == '.DS_Store' or d == 'BatchRank':
                        continue
                    if d not in results.keys():
                        results[d] = []

                    methods_name = os.listdir(dir_name + d)
                    for mn in methods_name:
                        if mn == 'DS_Store':
                            continue
                        d_name = float(mn.split('-')[0])
                        results[d].append([d_name, np.load(dir_name + d + '/' + mn)])

            plt.subplot(1,2,nn)
            nn+=1
            for idx, bkey in enumerate(['BubbleRank']):
                data_r = {}
                for item in results[bkey]:
                    key = str(np.floor(item[0] * 10) / 10)
                    if key not in data_r.keys():
                        data_r[key] = []
                    data_r[key].append(item[1][-1])
                keys = [key for key in data_r.keys() if float(key) > 9 and float(key) < 36]
                print [[key, len(data_r[key]), np.mean(data_r[key])] for key in sorted(keys)]
                print np.sum([len(data_r[key]) for key in keys])
                x = [float(key) for key in keys]
                y = [np.mean(data_r[key]) for key in keys]
                st = [np.std(data_r[key]) / len(data_r[key]) for key in keys]
                y_ = [max(y[i] - st[i], 0) for i in range(len(y))]
                y_min = [y[i] - y_[i] for i in range(len(y))]
                plt.errorbar(x, y, yerr=[y_min, st], fmt='.', markersize=3)
                if user == 'CM':
                    plt.ylabel('Regret')
                plt.xlabel('Number of swaps')
                plt.title(user)
                axes = plt.gca()
                if user == 'CM':
                    # plt.yticks(1000 * np.arange(0, 13, 2), ["0", "2k", "4k", "6k", "8k", "10k", "12k"])
                    axes.set_ylim([0, 8000])
                    plt.yticks(1000 * np.arange(0, 9, 2), ["0", "2k", "4k", "6k", "8k"])
                else:
                    axes.set_ylim([0, 40000])
                    # plt.yticks(10000 * np.arange(0, 9, 2), ["0", "10k", "40k", "60k", "80k"])
                    plt.yticks(10000 * np.arange(0, 5), ["0", "10k", "20k", "30k", "40k"])

        plt.tight_layout(pad=0.2)
        plt.savefig('figures/different-initials/' + 'all-Bubble'  + '@' + str(n_top) + '-dots.pdf', dpi=1200, bbox_inches=0)
        plt.savefig('figures/different-initials/' + 'all-Bubble' + '@' + str(n_top) + '-dots')


if __name__ == '__main__':
    QUERY = os.listdir('results/10020000/CM5/KLUCB')
    if '.DS_Store' in QUERY:
        QUERY.pop(QUERY.index('.DS_Store'))
    print len(QUERY)
    # QUERY = ['q82523']
    # for q in QUERY:
    #     if q == '.DS_Store':
    #         continue
    #     draw_ave(q)
        # draw(q)
    # draw_all()
    draw_all_curves()