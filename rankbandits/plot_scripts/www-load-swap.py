import os
import pickle

"""
Process on the data from slurm and transfer them to .pkl files.
For the swap results. 
"""

def load_swap(user, n_top, iteration=5000000, scale=10):
    # Result format: regret, nDCG, nDCG_power_rel, conservative, strong conservative
    results = {}
    results['BubbleRank'] = {}
    results['BubbleRank']['swap']=[]
    for q in QUERY:
        dir_name = '../results/different-initial-%d/' % iteration + user + str(n_top) + '/KLUCB/' + q + '/'
        dirs = os.listdir(dir_name)
        for d in dirs:
            if d == '.DS_Store':
                continue
            methods_name = os.listdir(dir_name + d)
            for mn in methods_name:
                if mn == 'DS_Store':
                    continue
                rt = pickle.load(open(dir_name + d + '/' + mn))
                results['BubbleRank']['swap'].append(rt)

    dir_name = '../results/different-initial-%d/' % iteration + user + str(n_top) + '.pkl'
    with open(dir_name, 'wb') as f:
        pickle.dump(results, f)
    return results


if __name__ == '__main__':
    data_names = ['DCM', 'CM', 'PBM']
    # data_names = ['DCM']
    for data_name in data_names:
        print data_name + '=' * 10
        iters = 5000000
        QUERY = os.listdir('../results/different-initial-%d/' % iters + data_name + '5/KLUCB')
        if '.DS_Store' in QUERY:
            QUERY.pop(QUERY.index('.DS_Store'))
        rt = load_swap(data_name, n_top=5, iteration=iters, scale=10)
