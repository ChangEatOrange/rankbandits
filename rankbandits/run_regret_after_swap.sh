#!/bin/sh

#SBATCH --mem=2G
#SBATCH -c4
#SBATCH -o ./out/%A.out                     
#SBATCH -e ./out/%A.err
#SBATCH -p cpu,mem

cd /zfs/ilps-plex1/slurm/datastore/cli1
source ./anaconda/bin/activate bandit_venv

cd /zfs/ilps-plex1/slurm/datastore/cli1/rankbandits/rankbandits

python run_regret_after_swap.py --data_idx=$1 --user=$2

source deactivate
